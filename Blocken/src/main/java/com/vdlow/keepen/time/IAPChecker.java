package com.vdlow.keepen.time;

import android.content.Context;
import android.util.Log;

import com.vdlow.keepen.billingsutils.IabException;
import com.vdlow.keepen.billingsutils.IabHelper;
import com.vdlow.keepen.billingsutils.IabResult;
import com.vdlow.keepen.billingsutils.Inventory;
import com.vdlow.keepen.billingsutils.Purchase;
import com.vdlow.keepen.classes.IAPService;

import java.util.ArrayList;

/**
 * Created by Vinicius Low on 31/01/14.
 */
public class IAPChecker {

    private Context context;
    private IabHelper mHelper;
    private Inventory inventory;

    public IAPChecker(Context context) {
        this.context = context;
    }

    public Boolean isPremium() {
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqt4Flvi1gMxJKuh23r8JWh0usa3VQnWDJVQFRJvW+KrOs5SGvKhUw9Q7S5WqmWoHsAjMEhfuwuyYeJdfLjMS3LkjZd0qHXrw3hU8faUMOSYmhPbRrZmKI8gqqOGRu+LwV5H5zQydocgD4qyCEIef2zp1pVXrUiTwEmjEPz0VbgBfXL47KOpi2+lc5mkxG/HdeP1dCbV/vNVQpD1urzIvs58585Zr+lXkaUBdRvgX+PdJ3s22a8KKoHB52uxP/MDB2FQ8HPQV/hhMnkofE5jGyrsXHXypmi3TsJ0DIYVGkl7gkOUp36nHxNBuFco3QAqVsoUsG7BY3lTCcefm3BjdJwIDAQAB";
        mHelper = new IabHelper(context, base64EncodedPublicKey);
        final Boolean[] isPremium = new Boolean[1];
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (result.isSuccess()) {
                    Log.d("IAP", "In-app Billing setup ok: " +
                            result);

                    ArrayList skus = new ArrayList();
                    skus.add(IAPService.ITEM_SKU);
                    try {
                        inventory =  mHelper.queryInventory(false, skus);
                        if(inventory != null){
                            Purchase proPurchase = inventory.getPurchase(IAPService.ITEM_SKU);
                            Boolean mIsPremium = (proPurchase != null);
                            if(mIsPremium){
                                isPremium[0] = true;
                            }
                            else{
                                isPremium[0] = false;
                            }
                        }
                    } catch (IabException e) {
                        Log.i("IabExpection addNtf", e.toString());
                        e.printStackTrace();
                        isPremium[0] = false;
                    }
                } else {
                    Log.d("IAP", "In-app Billing setup failed: " +
                            result);
                }
            }
        });
        while(isPremium[0] == null){}
        mHelper.dispose();
        return isPremium[0];
    }
}
