package com.vdlow.keepen.ui.fragments;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.ApplicationToBlockDAO;
import com.vdlow.keepen.classes.DAO.NotificationBlockedDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.ApplicationToBlock;
import com.vdlow.keepen.ui.adapters.AppNtfAdapter;

import java.util.List;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class NtfsPerAppViewer extends ListFragment {
    private PackageManager pm;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private List<ApplicationToBlock> apps;
    private SharedPreferences prefs;
    private List<PackageInfo> packs;
    private ProgressDialog dialog;
    private Bundle data;
    private String mode;
    private String isBlocked;
    private Boolean selected;
    private ApplicationToBlockDAO appDAO;
    private NotificationBlockedDAO ntfDAO;


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        if (getActivity().findViewById(R.id.container_detail) != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            SelectLeftFragment slf = new SelectLeftFragment();
            ft.replace(R.id.container_detail, slf, "AppManager");
            ft.commit();
        }

        try{
            data = getArguments();
            mode = data.getString("mode");
            if(mode.equals("block")) {
                isBlocked = "1";
            }
            else if(mode.equals("history")){
                isBlocked = "0";
            }
        }
        catch(Exception e){
            Log.i("AppManager error getting mode", e.toString());
            e.printStackTrace();
            Toast.makeText(getActivity(), getString(R.string.error_loading_apps), Toast.LENGTH_LONG).show();
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        showApps();

    }

    public void showApps() {
        ntfDAO = new NotificationBlockedDAO(getActivity());
        apps = ntfDAO.getApps(isBlocked);
        ApplicationToBlock thisApp;

        AppNtfAdapter adapter = new AppNtfAdapter(getActivity(), apps);
        adapter.setIsBlocked(isBlocked);
        setListAdapter(adapter);

        getListView().setDivider(null);
        getListView().setDividerHeight(0);
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        ApplicationToBlock app = (ApplicationToBlock)l.getAdapter().getItem(position);
        Bundle i = new Bundle();
        i.putString("package", app.getPackageName());
        i.putString("mode", mode);
        i.putString("groupBy", "perApp");
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        NotificationsViewer nv = new NotificationsViewer();
        nv.setArguments(i);
        ft.replace(com.vdlow.keepen.R.id.container, nv, "NtfListFromPerApp");
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        return inflater.inflate(R.layout.empty_listfragment_no_undo, null);
    }
}
