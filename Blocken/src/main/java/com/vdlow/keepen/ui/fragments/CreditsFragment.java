package com.vdlow.keepen.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vdlow.keepen.R;

/**
 * Created by Vinicius Low on 04/01/14.
 */
public class CreditsFragment extends Fragment {
    private NavigationDrawerFragment mNavigationDrawerFragment;
    @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        View view = inflater.inflate(R.layout.fragment_credits, container, false);

        return view;
    }
}
