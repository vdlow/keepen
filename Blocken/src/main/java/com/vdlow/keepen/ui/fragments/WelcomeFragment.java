package com.vdlow.keepen.ui.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.vdlow.keepen.R;
import com.vdlow.keepen.billingsutils.IabHelper;
import com.vdlow.keepen.billingsutils.IabResult;
import com.vdlow.keepen.billingsutils.Purchase;
import com.vdlow.keepen.classes.IAPService;
import com.vdlow.keepen.ui.activities.MainActivity;

/**
 * Created by Vinicius Low on 07/02/14.
 */
public class WelcomeFragment extends Fragment {
        public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
        private SharedPreferences prefs;
        private SharedPreferences.Editor editor;

        public static final WelcomeFragment newInstance(String message)
        {
            WelcomeFragment f = new WelcomeFragment();
            Bundle bdl = new Bundle(1);
            bdl.putString(EXTRA_MESSAGE, message);
            f.setArguments(bdl);
            return f;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            String message = getArguments().getString(EXTRA_MESSAGE);
            View v = inflater.inflate(R.layout.fragment_welcome, container, false);
            /*TextView messageTextView = (TextView)v.findViewById(R.id.txt1);
            messageTextView.setVisibility(View.VISIBLE);
            messageTextView.setText(message);*/

            if(message.equals("1")){
                ((ScrollView)v.findViewById(R.id.firstScreen)).setVisibility(View.VISIBLE);
            }
            else if (message.equals("2")) {
                ((ScrollView)v.findViewById(R.id.secondScreen)).setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT < 18) {
                    ((TextView) v.findViewById(R.id.txtActivate)).setText(getString(R.string.activate_accessibility));
                    ((ImageView) v.findViewById(R.id.imgActivate)).setImageResource(R.drawable.accessibility_service_2);
                    ((Button) v.findViewById(R.id.btnActivate)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent= new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                            startActivity(intent);
                        }
                    });
                } else {
                    ((Button) v.findViewById(R.id.btnActivate)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent= new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                            startActivity(intent);
                        }
                    });
                }
            }
            else if (message.equals("3")) {
                ((ScrollView)v.findViewById(R.id.thirdScreen)).setVisibility(View.VISIBLE);
            }
            else if (message.equals("4")) {
                ((ScrollView)v.findViewById(R.id.fourthScreen)).setVisibility(View.VISIBLE);
            }
            else if (message.equals("5")) {
                ((ScrollView)v.findViewById(R.id.fifthScreen)).setVisibility(View.VISIBLE);
                ((Button) v.findViewById(R.id.btnGoToApp)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent= new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);

                        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                        editor = prefs.edit();

                        editor.putBoolean("firstUseTutorial", true);
                        editor.commit();

                        getActivity().finish();
                    }
                });
            }

            return v;
        }
}
