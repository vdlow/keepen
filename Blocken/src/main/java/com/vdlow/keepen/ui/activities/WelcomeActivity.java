package com.vdlow.keepen.ui.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.astuetz.PagerSlidingTabStrip;
import com.vdlow.keepen.R;
import com.vdlow.keepen.billingsutils.IabHelper;
import com.vdlow.keepen.billingsutils.IabResult;
import com.vdlow.keepen.billingsutils.Purchase;
import com.vdlow.keepen.classes.IAPService;
import com.vdlow.keepen.ui.fragments.WelcomeFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vinicius Low on 07/02/14.
 */
public class WelcomeActivity extends FragmentActivity {
    MyPageAdapter pageAdapter;
    private IabHelper mHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        List<Fragment> fragments = getFragments();
        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
        ViewPager pager;
        pager = (ViewPager)findViewById(R.id.viewpager);
        pager.setAdapter(pageAdapter);

        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setShouldExpand(true);

        tabs.setViewPager(pager);
    }

    private List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();

        fList.add(WelcomeFragment.newInstance("1"));
        fList.add(WelcomeFragment.newInstance("2"));
        fList.add(WelcomeFragment.newInstance("3"));
        fList.add(WelcomeFragment.newInstance("4"));
        fList.add(WelcomeFragment.newInstance("5"));

        return fList;
    }

    class MyPageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;
        private final String title = "";


        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return title;
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }

    public void goPro(View view) {
        IAPService.goPro(this);
    }
}
