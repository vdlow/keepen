package com.vdlow.keepen.ui.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.DateUtils;
import com.vdlow.keepen.classes.NotificationBlocked;

import java.util.List;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class NotificationBlockedAdapter extends ArrayAdapter<NotificationBlocked> {

    private final Context context;
    private final List<NotificationBlocked> values;
    private NotificationBlocked ntf;

    public NotificationBlockedAdapter(Context context, List<NotificationBlocked> values) {
        super(context, R.layout.notification_blocked_adapter, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ntfRow = inflater.inflate(R.layout.notification_blocked_adapter, parent, false);

        final ImageView appIcon = (ImageView) ntfRow.findViewById(R.id.appIcon);
        final TextView ntfTitle = (TextView) ntfRow.findViewById(R.id.ntfTitle);
        final TextView appName = (TextView) ntfRow.findViewById(R.id.appName);
        final TextView blockDate = (TextView) ntfRow.findViewById(R.id.blockDate);
        final ImageView muteIcon = (ImageView) ntfRow.findViewById(R.id.muteIcon);

        ntf = values.get(position);

        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo;
        try {
            applicationInfo = packageManager.getApplicationInfo(ntf.getApp().getPackageName(), 0);

            appIcon.setImageDrawable(packageManager.getApplicationIcon(applicationInfo));
        }
        catch (Exception e){
            Log.e("Get app icon Exception", e.toString());
            e.printStackTrace();

            appIcon.setImageResource(R.drawable.default_icon_ntf);
        }

        if (ntf.getNtfTitle() == null || ntf.getNtfTitle().trim().length() == 0) {
            ntfTitle.setText(context.getString(R.string.touchToSeeMore));
        } else {
            ntfTitle.setText(ntf.getNtfTitle());
        }

        appName.setText(ntf.getApp().getAppName());
        blockDate.setText(DateUtils.getDate(ntf.getNtfDate(), getContext()));
        Bundle teste = new Bundle();

        if (ntf.getIsMutedNtf() == 1) {
            muteIcon.setVisibility(View.VISIBLE);
        } else {
            muteIcon.setVisibility(View.INVISIBLE);
        }

        return ntfRow;
    }

    public NotificationBlocked getNtf(){
        return ntf;
    }
}
