package com.vdlow.keepen.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.ApplicationToBlockDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.ApplicationToBlock;

import java.util.Calendar;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class AppPickerAdapter extends ArrayAdapter<ApplicationToBlock> {

    private ApplicationToBlock[] values;
    private ApplicationToBlock app;
    private LayoutInflater inflater;

    public AppPickerAdapter(Context context, ApplicationToBlock[] values) {
        super(context, R.layout.app_picker_adapter, values);
        this.values = values;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View appRow = inflater.inflate(R.layout.app_picker_adapter, parent, false);

        final ImageView appIcon = (ImageView) appRow.findViewById(R.id.appIcon);
        final TextView appTitle = (TextView) appRow.findViewById(R.id.appTitle);
        final TextView appPack = (TextView) appRow.findViewById(R.id.appPackage);
        final CheckBox block = (CheckBox) appRow.findViewById(R.id.chkBlock);
        final TextView blockDate = (TextView) appRow.findViewById(R.id.blockDate);

        app = values[position];

        appIcon.setImageDrawable(app.getIcon());
        appTitle.setText(app.getAppName());
        appPack.setText(app.getPackageName());
        block.setChecked(app.isBlocked());
        blockDate.setText(app.getDateBlocked());

        block.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ApplicationToBlockDAO appDao = new ApplicationToBlockDAO(getContext());
                try {
                    appDao.open();
                    if (isChecked) {
                        boolean inserted = appDao.addAppBlock(appPack.getText().toString(), appTitle.getText().toString(), app.getTable());
                        if (!inserted) {
                            Toast.makeText(getContext(), getContext().getString(R.string.app_already_blocked), Toast.LENGTH_LONG);
                        } else {
                            Calendar cal = Calendar.getInstance();
                            String dataFinal = String.valueOf(
                                    cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) +
                                            " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE)
                            );
                            blockDate.setText(dataFinal);
                        }
                    } else {
                        appDao.deleteAppBlock(appPack.getText().toString(), app.getTable());
                        blockDate.setText("");
                    }
                } catch (Exception e) {
                    Log.e("Database Error", e.toString());
                }
            }
        });

        return appRow;
    }
}

