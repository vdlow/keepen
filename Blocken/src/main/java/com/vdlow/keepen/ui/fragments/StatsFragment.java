package com.vdlow.keepen.ui.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vdlow.keepen.classes.DAO.ApplicationToBlockDAO;
import com.vdlow.keepen.classes.DAO.BlockedWordDAO;
import com.vdlow.keepen.classes.DAO.MySQLiteHelper;
import com.vdlow.keepen.classes.DAO.NotificationBlockedDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.ui.view.StatView;

/**
 * Created by Vinicius Low on 04/01/14.
 */
public class StatsFragment extends Fragment {
    private NotificationBlockedDAO ntfDAO;
    private ApplicationToBlockDAO appDAO;
    private BlockedWordDAO wordDAO;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private View view;
    private LinearLayout layout;
    private TextView labelTitle;

    @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        if (getActivity().findViewById(R.id.container_detail) != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            SelectLeftFragment slf = new SelectLeftFragment();
            slf.hideText();
            ft.replace(R.id.container_detail, slf, "AppManager");
            ft.commit();
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        view = inflater.inflate(R.layout.fragment_stats, container, false);

        return view;
    }

    public void loadDataInViews(){

        layout = (LinearLayout) view.findViewById(R.id.statsLayout);

        labelTitle = (TextView) view.findViewById(R.id.titleStats);

        layout.removeAllViews();

        layout.addView(labelTitle);

        ntfDAO = new NotificationBlockedDAO(getActivity());
        appDAO = new ApplicationToBlockDAO(getActivity());
        wordDAO = new BlockedWordDAO(getActivity());

        StatView stat = new StatView(getActivity());
        stat.setLabel(getString(R.string.db_size));
        stat.setValue(ntfDAO.getDbSize()+"MB");
        layout.addView(stat);

        stat = new StatView(getActivity());
        stat.setLabel(getString(R.string.ntf_dismissed));
        stat.setValue(String.valueOf(ntfDAO.getNtfsNumber(1, "")));
        layout.addView(stat);

        stat = new StatView(getActivity());
        stat.setLabel(getString(R.string.ntf_logged));
        stat.setValue(String.valueOf(ntfDAO.getNtfsNumber(0, "")));
        layout.addView(stat);

        stat = new StatView(getActivity());
        stat.setLabel(getString(R.string.apps_blocked));
        stat.setValue(String.valueOf(appDAO.getNumberApps(MySQLiteHelper.TABLE_BLOCKEDAPPS)));
        layout.addView(stat);

        int nWords = wordDAO.getBlockedWordsNumber();
        if (nWords > 0) {
            stat = new StatView(getActivity());
            stat.setLabel(getString(R.string.words_blocked));
            stat.setValue(String.valueOf(nWords));
            layout.addView(stat);
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        loadDataInViews();
    }
}
