package com.vdlow.keepen.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.BlockedWordDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.BlockedWord;

import java.util.List;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class BlockedWordAdapter extends ArrayAdapter<BlockedWord> {

    private final Context context;
    private final List<BlockedWord> values;
    private BlockedWord word;
    private BlockedWordDAO wordDAO;

    public BlockedWordAdapter(Context context, List<BlockedWord> values) {
        super(context, R.layout.blocked_word_adapter, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View wordRow = inflater.inflate(R.layout.blocked_word_adapter, parent, false);

        final TextView txtWord = (TextView) wordRow.findViewById(R.id.word);
        final TextView blockDate = (TextView) wordRow.findViewById(R.id.blockDate);
        final CheckBox chkTitleOnly = (CheckBox) wordRow.findViewById(R.id.chkTitleOnly);

        word = values.get(position);

        txtWord.setText(word.getWord());
        blockDate.setText(word.getDate());
        chkTitleOnly.setChecked(word.getTitleOnly());

        chkTitleOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wordDAO = new BlockedWordDAO(BlockedWordAdapter.this.getContext());
                word = getItem(position);
                word.setTitleOnly(isChecked);
                if (!wordDAO.changeTitleOnly(word)) {
                    Toast.makeText(BlockedWordAdapter.this.getContext(), BlockedWordAdapter.this.context.getString(R.string.error_updating_word), Toast.LENGTH_LONG).show();
                }
            }
        });

        return wordRow;
    }

    public BlockedWord getWord(){
        return word;
    }
}
