package com.vdlow.keepen.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vdlow.keepen.R;

/**
 * Created by Vinicius Low on 04/01/14.
 */
public class SelectLeftFragment extends Fragment {
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Boolean hideText = false;

    @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        View view = inflater.inflate(R.layout.select_left_fragment, container, false);

        if (hideText) {
            view.findViewById(R.id.selectLeftText).setVisibility(View.INVISIBLE);
        }

        return view;
    }

    public void hideText(){
        hideText = true;
    }
}
