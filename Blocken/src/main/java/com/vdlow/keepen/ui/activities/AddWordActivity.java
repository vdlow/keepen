package com.vdlow.keepen.ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.BlockedWordDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.BlockedWord;

import java.util.Calendar;

/**
 * Created by Vinicius Low on 18/01/14.
 */
public class AddWordActivity extends Activity {

    private BlockedWordDAO wordDAO;
    private BlockedWord word;
    private EditText txtWord;
    private CheckBox chkTitleOnly;
    private ImageButton btnSave;
    private SharedPreferences prefs;
    private AlertDialog.Builder dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addword);

        txtWord = (EditText) findViewById(R.id.txtWord);
        chkTitleOnly = (CheckBox) findViewById(R.id.chkTitleOnly);
        btnSave = (ImageButton) findViewById(R.id.imgBtnSave);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (!prefs.getBoolean("premiumpackage221293", false)) {
            txtWord.setEnabled(false);
            chkTitleOnly.setEnabled(false);
            btnSave.setEnabled(false);

            dialog = new AlertDialog.Builder(this);
            dialog.setTitle(getString(R.string.app_name));
            dialog.setMessage(getString(R.string.pro_only_feature));
            dialog.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dialog.show();
        }
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type) && intent.getStringExtra(Intent.EXTRA_TEXT) != null) {
                txtWord.setText(intent.getStringExtra(Intent.EXTRA_TEXT));
            }
        }
    }

    public void saveWord(View v){
        wordDAO = new BlockedWordDAO(this);
        word = new BlockedWord();

        word.setTitleOnly(chkTitleOnly.isChecked());
        word.setWord(txtWord.getText().toString());

        Calendar cal = Calendar.getInstance();
        String dataFinal = String.valueOf(
                cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) +
                        " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE)
        );

        word.setDate(dataFinal);

        String result = wordDAO.addBlockedWord(word, AddWordActivity.this);

        Toast.makeText(this, result, Toast.LENGTH_LONG).show();

        if (result.equals(getString(R.string.word_added))) {
            finish();
        }
    }

    public void cancel(View v){
        finish();
    }
}
