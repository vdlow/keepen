package com.vdlow.keepen.ui.fragments;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;

import com.vdlow.keepen.R;
import com.vdlow.keepen.billingsutils.IabHelper;
import com.vdlow.keepen.billingsutils.IabResult;
import com.vdlow.keepen.billingsutils.Purchase;
import com.vdlow.keepen.classes.IAPService;
import com.vdlow.keepen.time.RadialPickerLayout;
import com.vdlow.keepen.time.TimePickerDialog;
import com.vdlow.keepen.ui.activities.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Vinicius Low on 01/02/14.
 */
public class GeneralPreferences extends PreferenceFragment {
    private IabHelper mHelper;
    private TimePickerDialog tpd;
    private TimePickerDialog.OnTimeSetListener tpdList;
    private DialogFragment dfg;
    private SharedPreferences prefs;
    private SharedPreferences.Editor prefsEditor;
    private AlertDialog.Builder dialog;
    private SimpleDateFormat dFormat;
    private String date;
    private Calendar c;
    private String settings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settings = getArguments().getString("settingsType");
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        if (settings.equals("dismiss")) {
            addPreferencesFromResource(R.xml.dismiss_preferences);
            if (Build.VERSION.SDK_INT < 18) {
                dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle(getString(R.string.app_name));
                dialog.setMessage(getString(R.string.version_not_supported));
                dialog.setNeutralButton(android.R.string.ok, null);
                dialog.show();
                getPreferenceScreen().setEnabled(false);
            }

            if (prefs.getBoolean("premiumpackage221293", false)) {
                getPreferenceScreen().findPreference("manageBlockedWords").setEnabled(true);
                getPreferenceScreen().findPreference("manageBlockedWords").setTitle(getString(R.string.action_manage_blocked_words));
            }

            getPreferenceScreen().findPreference("manageBlockedApps").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("mode", "block");
                    startActivity(i);
                    return true;
                }
            });

            getPreferenceScreen().findPreference("manageBlockedWords").setTitle(getPreferenceScreen().findPreference("manageBlockedWords").getTitle() + " (PRO)");

            getPreferenceScreen().findPreference("manageBlockedWords").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("mode", "wordsMng");
                    startActivity(i);
                    return false;
                }
            });

        }
        else if (settings.equals("history")) {
            addPreferencesFromResource(R.xml.history_preferences);
            if (Build.VERSION.SDK_INT < 18) {
                getPreferenceScreen().findPreference("logPersistentNtfs").setEnabled(false);
            }

            getPreferenceScreen().findPreference("manageNoLogApps").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("mode", "history");
                    startActivity(i);
                    return true;
                }
            });
        }
        else if (settings.equals("mute")) {
            addPreferencesFromResource(R.xml.mute_preferences);
            if (Build.VERSION.SDK_INT < 18) {
                dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle(getString(R.string.app_name));
                dialog.setMessage(getString(R.string.version_not_supported));
                dialog.setNeutralButton(android.R.string.ok, null);
                dialog.show();
                getPreferenceScreen().setEnabled(false);
            }
            if (prefs.getBoolean("premiumpackage221293", false)) {
                getPreferenceScreen().setEnabled(true);
            } else {
                dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle(getString(R.string.app_name));
                dialog.setMessage(getString(R.string.pro_only_feature));
                dialog.setNeutralButton(android.R.string.ok, null);
                dialog.show();
            }

            if(prefs.getInt("startMuteHour", -1) >= 0 && prefs.getInt("startMuteMinute", -1) >= 0
                    && prefs.getInt("stopMuteHour", -1) >= 0 && prefs.getInt("stopMuteMinute", -1) >= 0) {
                updateTimeSummary();
            }

            getPreferenceScreen().findPreference("muteActive").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    if(prefs.getInt("startMuteHour", -1) < 0){
                        showTimePicker("startMute");
                    }
                    return true;
                }
            });


            getPreferenceScreen().findPreference("muteTime").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    showTimePicker("startMute");
                    return true;
                }
            });

        }
        else if (settings.equals("general")) {
            addPreferencesFromResource(R.xml.general_preferences);
            getPreferenceScreen().findPreference("maxDBSize").setTitle(getPreferenceScreen().findPreference("maxDBSize").getTitle() + " (PRO)");

            String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqt4Flvi1gMxJKuh23r8JWh0usa3VQnWDJVQFRJvW+KrOs5SGvKhUw9Q7S5WqmWoHsAjMEhfuwuyYeJdfLjMS3LkjZd0qHXrw3hU8faUMOSYmhPbRrZmKI8gqqOGRu+LwV5H5zQydocgD4qyCEIef2zp1pVXrUiTwEmjEPz0VbgBfXL47KOpi2+lc5mkxG/HdeP1dCbV/vNVQpD1urzIvs58585Zr+lXkaUBdRvgX+PdJ3s22a8KKoHB52uxP/MDB2FQ8HPQV/hhMnkofE5jGyrsXHXypmi3TsJ0DIYVGkl7gkOUp36nHxNBuFco3QAqVsoUsG7BY3lTCcefm3BjdJwIDAQAB";
            mHelper = new IabHelper(getActivity(), base64EncodedPublicKey);

            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                @Override
                public void onIabSetupFinished(IabResult result) {
                    if (result.isSuccess()) {
                        Log.d("IAP", "In-app Billing setup ok: " +
                                result);
                    } else {
                        Log.d("IAP", "In-app Billing setup failed: " +
                                result);
                    }
                }
            });

            findPreference("goPro").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    IAPService.goPro(getActivity());
                    return true;
                }
            });

            findPreference("credits").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {

                    Intent i = new Intent(getActivity(), MainActivity.class);
                    i.putExtra("mode", "credits");
                    startActivity(i);
                    return true;
                }
            });
            if (prefs.getBoolean("premiumpackage221293", false)) {
                getPreferenceScreen().findPreference("goPro").setEnabled(false);
                getPreferenceScreen().findPreference("maxDBSize").setEnabled(true);
                getPreferenceScreen().findPreference("maxDBSize").setTitle(getString(R.string.db_size_title));
            }
        }

    }

    public void showTimePicker(final String preferenceKey){
        //preferenceKey should either be startMute or stopMute
        tpd = new TimePickerDialog();
        tpdList = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                prefsEditor = prefs.edit();
                prefsEditor.putInt(preferenceKey + "Hour", hourOfDay);
                prefsEditor.putInt(preferenceKey + "Minute", minute);
                prefsEditor.commit();
                if(preferenceKey.equals("startMute")) {
                    showTimePicker("stopMute");
                }
                else if(preferenceKey.equals("stopMute")) {
                    updateTimeSummary();
                }
            }
        };
        tpd.initialize(tpdList, 0, 0, DateFormat.is24HourFormat(getActivity()));
        dfg = tpd;
        dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.app_name));
        if(preferenceKey.equals("startMute")) {
            dialog.setMessage(getString(R.string.set_mute_start));
        }
        else if(preferenceKey.equals("stopMute")) {
            dialog.setMessage(getString(R.string.set_mute_stop));
        }
        dialog.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dfg.show(getFragmentManager(), "timePicker");
            }
        });
        dialog.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //DO NOTHING
            }
        });
        dialog.show();
    }

    public void updateTimeSummary(){
        if(DateFormat.is24HourFormat(getActivity())){
            dFormat = new SimpleDateFormat("H:mm");
        }
        else{
            dFormat = new SimpleDateFormat("h:mm a");
        }

        date = getString(R.string.start) + " ";
        c = Calendar.getInstance();
        c.set(0, 0, 0, prefs.getInt("startMuteHour", -1), prefs.getInt("startMuteMinute", -1));
        date = date + dFormat.format(c.getTime()) + "\n" + getString(R.string.stop) + " ";
        c.set(0, 0, 0, prefs.getInt("stopMuteHour", -1), prefs.getInt("stopMuteMinute", -1));
        date = date + dFormat.format(c.getTime());

        getPreferenceScreen().findPreference("muteTime").setSummary(date);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (settings.equals("mute")) {
            updateTimeSummary();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (settings.equals("general")) {
            mHelper.dispose();
        }
    }
}
