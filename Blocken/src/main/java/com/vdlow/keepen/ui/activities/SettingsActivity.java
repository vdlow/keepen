package com.vdlow.keepen.ui.activities;


import android.annotation.TargetApi;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.vdlow.keepen.R;
import com.vdlow.keepen.billingsutils.IabHelper;

import java.util.List;

/**
 * Created by Vinicius Low on 04/01/14.
 */
public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preferences, target);
    }

    @Override
    @TargetApi(19)
    protected boolean isValidFragment(String fragmentName) {
        if (fragmentName.startsWith("com.vdlow.keepen")) {
            return true;
        }
        return super.isValidFragment(fragmentName);
    }
}
