package com.vdlow.keepen.ui.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vdlow.keepen.R;
import com.vdlow.keepen.billingsutils.IabHelper;
import com.vdlow.keepen.billingsutils.IabResult;
import com.vdlow.keepen.billingsutils.Purchase;
import com.vdlow.keepen.classes.IAPService;
import com.vdlow.keepen.ratingdialog.RateMyApp;
import com.vdlow.keepen.ratingdialog.RateMyAppBuilder;
import com.vdlow.keepen.ui.fragments.AppManager;
import com.vdlow.keepen.ui.fragments.BlockedWordsManager;
import com.vdlow.keepen.ui.fragments.CreditsFragment;
import com.vdlow.keepen.ui.fragments.NavigationDrawerFragment;
import com.vdlow.keepen.ui.fragments.NotificationFragment;
import com.vdlow.keepen.ui.fragments.NotificationsViewer;
import com.vdlow.keepen.ui.fragments.NtfsPerAppViewer;
import com.vdlow.keepen.ui.fragments.StatsFragment;

import java.util.Calendar;

public class MainActivity extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private SharedPreferences prefs;
    private FragmentManager fragmentManager = getFragmentManager();
    private AppManager appMng;
    private Bundle data;
    private FragmentTransaction ft;
    private NotificationsViewer ntfViewer;
    private Intent i;
    private NtfsPerAppViewer ntfsPerAppViewer;
    private IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if(!prefs.getBoolean("firstUseTutorial", false)){
            i = new Intent(MainActivity.this, WelcomeActivity.class);
            startActivity(i);
            finish();
        }
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getString(R.string.app_name);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        try{
            i = getIntent();
            if(i != null){
                if(i.getStringExtra("mode") != null && (i.getStringExtra("mode").equals("block") || i.getStringExtra("mode").equals("history"))){
                    openAppManager(i.getStringExtra("mode"));
                }
                else if(i.getStringExtra("mode") != null && i.getStringExtra("mode").equals("wordsMng")){
                    openBlockedWordsManager();
                }
                else if(i.getStringExtra("mode") != null && i.getStringExtra("mode").equals("credits")){
                    openCreditsScreen();
                }
            }
        }
        catch(Exception e){
            Log.e("MainActivity - error getting intent", e.toString());
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();

        Intent intentServiceIAP = new Intent(this, IAPService.class);
        startService(intentServiceIAP);
        PendingIntent pintent = PendingIntent.getService(this, 0, intentServiceIAP, 0);

        AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 8640*10000, pintent);

        RateMyAppBuilder builder = new RateMyAppBuilder();
        builder.setLaunchesBeforeAlert(20);
        builder.setDaysBeforeAlert(20);
        RateMyApp rateMyApp = builder.build(this);
        rateMyApp.appLaunched();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments

        switch(position){
            case 0:
                ntfViewer = new NotificationsViewer();
                data = new Bundle();
                data.putString("mode", "history");
                ntfViewer.setArguments(data);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ntfViewer, "LoggedNotificationsViewer")
                        .commit();
                break;
            case 1:
                ntfsPerAppViewer = new NtfsPerAppViewer();
                data = new Bundle();
                data.putString("mode", "history");
                ntfsPerAppViewer.setArguments(data);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ntfsPerAppViewer, "NotificationsPerAppViewer")
                        .commit();
                break;
            case 2:
                ntfViewer = new NotificationsViewer();
                data = new Bundle();
                data.putString("mode", "block");
                ntfViewer.setArguments(data);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ntfViewer, "NotificationsViewer")
                        .commit();
                break;
            case 3:
                ntfsPerAppViewer = new NtfsPerAppViewer();
                data = new Bundle();
                data.putString("mode", "block");
                ntfsPerAppViewer.setArguments(data);
                fragmentManager.beginTransaction()
                        .replace(R.id.container, ntfsPerAppViewer, "NotificationsPerAppViewer")
                        .commit();
                break;
            case 4:
                i = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(i);
                break;
            case 5:
                fragmentManager.beginTransaction()
                        .replace(R.id.container, new StatsFragment(), "StatsFragment")
                        .commit();
                break;
            case 6:
                IAPService.goPro(this);
                break;

        }
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.app_name);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            //getMenuInflater().inflate(R.menu.main, menu);
            getMenuInflater().inflate(R.menu.onoffbutton, menu);

            prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if(prefs.getBoolean("blockModeActive", true)){
                menu.findItem(R.id.onOffBtn).setTitle(R.string.blocking_on);
            }
            if(prefs.getBoolean("historyModeActive", true)){
                menu.findItem(R.id.onOffBtnHistory).setTitle(R.string.history_on);
            }
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

    public void openAppManager(String mode){
        data = new Bundle();
        data.putString("mode", mode);
        ft= getFragmentManager().beginTransaction();
        appMng = new AppManager();
        appMng.setArguments(data);
        ft.replace(R.id.container, appMng, "AppManager");
        ft.commit();
    }

    public void openBlockedWordsManager(){
        ft = getFragmentManager().beginTransaction();
        BlockedWordsManager wordsMng = new BlockedWordsManager();
        ft.replace(R.id.container, wordsMng, "BlockedWordsManager");
        ft.commit();
    }

    public void openCreditsScreen(){
        ft = getFragmentManager().beginTransaction();
        CreditsFragment cFrag = new CreditsFragment();
        ft.replace(R.id.container, cFrag, "CreditsFragment");
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (
                (NotificationFragment) getFragmentManager().findFragmentByTag("NtfFragment") != null ||
                        (NotificationsViewer) getFragmentManager().findFragmentByTag("NtfListFromPerApp") != null
                ) {
            getFragmentManager().popBackStackImmediate();
        }
        else{
            super.onBackPressed();
        }
    }
}
