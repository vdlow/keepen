package com.vdlow.keepen.ui.adapters;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vdlow.keepen.classes.DAO.NotificationBlockedDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.ApplicationToBlock;

import java.util.List;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class AppNtfAdapter extends ArrayAdapter<ApplicationToBlock> {

    private final Context context;
    private final List<ApplicationToBlock> values;
    private ApplicationToBlock app;
    private NotificationBlockedDAO ntfDAO;
    private String isBlocked;
    private String type;

    public AppNtfAdapter(Context context, List<ApplicationToBlock> values) {
        super(context, R.layout.app_ntf_adapter, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View appRow = inflater.inflate(R.layout.app_ntf_adapter, parent, false);

        final ImageView appIcon = (ImageView) appRow.findViewById(R.id.appIcon);
        final TextView appTitle = (TextView) appRow.findViewById(R.id.appTitle);
        final TextView appPack = (TextView) appRow.findViewById(R.id.appPackage);
        final TextView countNtf = (TextView) appRow.findViewById(R.id.countNtf);

        app = values.get(position);
        ntfDAO = new NotificationBlockedDAO(getContext());

        try {
            appIcon.setImageDrawable(getContext().getPackageManager().getApplicationIcon(app.getPackageName()));
        } catch (PackageManager.NameNotFoundException e) {
            Log.i("Error getting app icon AppNtfAdapter", e.toString());
            e.printStackTrace();
            appIcon.setImageDrawable(getContext().getResources().getDrawable(R.drawable.default_icon_ntf));
        }
        appTitle.setText(app.getAppName());
        appPack.setText(app.getPackageName());
        if (isBlocked.equals("1")) {
            type = " " + getContext().getString(R.string.ntf_dismissed);
        } else {
            type = " " + getContext().getString(R.string.ntf_logged);
        }
        countNtf.setText(String.valueOf(ntfDAO.getNtfsNumber(Integer.parseInt(isBlocked), app.getPackageName()) + type));


        return appRow;
    }

    public void setIsBlocked(String isBlocked) {
        this.isBlocked = isBlocked;
    }
}
