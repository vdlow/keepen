package com.vdlow.keepen.ui.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.ApplicationToBlockDAO;
import com.vdlow.keepen.classes.DAO.MySQLiteHelper;
import com.vdlow.keepen.classes.DAO.NotificationBlockedDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.ApplicationToBlock;
import com.vdlow.keepen.classes.DateUtils;
import com.vdlow.keepen.classes.NotificationBlocked;

/**
 * Created by Vinicius Low on 04/01/14.
 */
public class NotificationFragment extends Fragment {
    private Bundle i;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ApplicationToBlock app;
    private ApplicationToBlockDAO appDAO;
    private NotificationBlocked ntf;
    private String mode;

    @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        final View view = inflater.inflate(R.layout.fragment_notification, container, false);
        try{
            ntf = new NotificationBlocked();
            app = new ApplicationToBlock();
            i = getArguments();
            ntf.setId(i.getInt("ntfID"));
            app.setAppName(i.getString("appName"));
            app.setPackageName(i.getString("ntfPackage"));
            ntf.setApp(app);
            ntf.setNtfTitle(i.getString("ntfTitle"));
            ntf.setNtfText(i.getString("ntfText"));
            ntf.setNtfDate(i.getString("ntfDate"));
            mode = i.getString("mode");
        }
        catch(Exception e){
            Log.i("Notification Fragment - getting data exception", e.toString());
            e.printStackTrace();
            Toast.makeText(getActivity(), getString(R.string.ntf_false_exists), Toast.LENGTH_LONG);
            getFragmentManager().popBackStackImmediate();
        }
        TextView appName = (TextView) view.findViewById(R.id.appName);
        RelativeLayout ntfTitleWrapper = (RelativeLayout) view.findViewById(R.id.ntfTitleWrapper);
        TextView ntfTitleLabel = (TextView) view.findViewById(R.id.ntfTitleLabel);
        TextView ntfTitle = (TextView) view.findViewById(R.id.ntfTitle);
        TextView ntfText = (TextView) view.findViewById(R.id.ntfText);
        TextView ntfDate = (TextView) view.findViewById(R.id.ntfDate);
        RelativeLayout ntfTextWrapper = (RelativeLayout) view.findViewById(R.id.ntfTextWrapper);
        TextView ntfTextLabel = (TextView) view.findViewById(R.id.ntfTextLabel);
        View dropShadowTitle = (View) view.findViewById(R.id.dropShadowTitle);
        View dropShadowText = (View) view.findViewById(R.id.dropShadowText);
        CheckBox chkBlockThisApp = (CheckBox) view.findViewById(R.id.chkBlockThisApp);

        appName.setText(ntf.getApp().getAppName());

        if(ntf.getNtfText() != null && ntf.getNtfText().trim().length()>0){
            ntfText.setText(ntf.getNtfText());
        }
        else{
            ntfTextWrapper.setVisibility(View.GONE);
            ntfTextWrapper.invalidate();
            ntfTextLabel.setVisibility(View.GONE);
            ntfTextLabel.invalidate();
            dropShadowText.setVisibility(View.GONE);
            dropShadowText.invalidate();
        }

        if (ntf.getNtfTitle() == null || ntf.getNtfTitle().equals(getString(R.string.touchToSeeMore))) {
            ntfTitleWrapper.setVisibility(View.GONE);
            ntfTitleWrapper.invalidate();
            ntfTitleLabel.setVisibility(View.GONE);
            ntfTitleLabel.invalidate();
            dropShadowTitle.setVisibility(View.GONE);
            dropShadowTitle.invalidate();
        } else {
            ntfTitle.setText(ntf.getNtfTitle());
        }

        ntfDate.setText(DateUtils.getDate(ntf.getNtfDate(), getActivity()));

        View.OnClickListener delete = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(view);
            }
        };

        View.OnClickListener recreate = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recreateNotification(view);
            }
        };

        View.OnClickListener back = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack(view);
            }
        };

        ((ImageButton)view.findViewById(R.id.btnDelete)).setOnClickListener(delete);
        ((RelativeLayout)view.findViewById(R.id.btnDeleteRelLayout)).setOnClickListener(delete);

        ((ImageButton)view.findViewById(R.id.btnBack)).setOnClickListener(back);
        ((RelativeLayout)view.findViewById(R.id.btnBackRelLayout)).setOnClickListener(back);

        ((ImageButton)view.findViewById(R.id.btnRenotify)).setOnClickListener(recreate);
        ((RelativeLayout)view.findViewById(R.id.btnRecreateRelLayout)).setOnClickListener(recreate);

        if(Build.VERSION.SDK_INT < 16){
            ((RelativeLayout)view.findViewById(R.id.btnRecreateRelLayout)).setVisibility(View.GONE);
        }

        if (mode.equals("history")) {
            app.setBlocked(getActivity(), MySQLiteHelper.TABLE_NOLOGAPPS);
            chkBlockThisApp.setText(getString(R.string.dont_save) + " " + app.getAppName() + " " + getString(R.string.notifications));
        }
        else if (mode.equals("blocked")) {
            app.setBlocked(getActivity(), MySQLiteHelper.TABLE_BLOCKEDAPPS);
            chkBlockThisApp.setText(getString(R.string.dismiss) + " " + app.getAppName() + " " + getString(R.string.notifications));
        }

        chkBlockThisApp.setChecked(app.isBlocked());

        chkBlockThisApp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                appDAO = new ApplicationToBlockDAO(getActivity().getApplicationContext());
                String table = "";
                if(mode.equals("history")) {
                    table = MySQLiteHelper.TABLE_NOLOGAPPS;
                }
                else if (mode.equals("blocked")) {
                    table = MySQLiteHelper.TABLE_BLOCKEDAPPS;
                }
                if(isChecked){
                    appDAO.addAppBlock(app.getPackageName(), app.getAppName(), table);
                }
                else{
                    appDAO.deleteAppBlock(app.getPackageName(), table);
                }
            }
        });

        return view;
    }

    @TargetApi(16)
    public void recreateNotification(View v){
        String titulo;
        if (ntf.getNtfTitle() != null) {
            titulo = ntf.getNtfTitle();
        }
        else {
            titulo = "";
        }
        Notification n = new Notification.Builder(getActivity())
                .setContentTitle(ntf.getApp().getAppName() + ": " + titulo)
                .setContentText(ntf.getNtfText())
                .setSmallIcon(R.drawable.ic_notification)
                .setStyle(new Notification.BigTextStyle().bigText(ntf.getNtfText()))
                .build();

        NotificationManager nm = (NotificationManager) getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);
        nm.notify(Integer.parseInt("103" + ntf.getId()), n);
    }

    public void goBack(View v){
        getFragmentManager().popBackStackImmediate();
    }

    public void delete(View v){
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.delete))
                .setMessage(getString(R.string.sure_delete))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        NotificationBlockedDAO ntfDAO = new NotificationBlockedDAO(getActivity());
                        ntfDAO.deleteNtfBlocked(i.getInt("ntfID", 0));
                        getFragmentManager().popBackStackImmediate();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }
}
