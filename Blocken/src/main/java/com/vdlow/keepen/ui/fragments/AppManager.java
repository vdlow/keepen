package com.vdlow.keepen.ui.fragments;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.ApplicationToBlockDAO;
import com.vdlow.keepen.classes.DAO.MySQLiteHelper;
import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.ApplicationToBlock;
import com.vdlow.keepen.ui.adapters.AppPickerAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class AppManager extends ListFragment {
    private PackageManager pm;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private ApplicationToBlock[] apps;
    private SharedPreferences prefs;
    private List<ResolveInfo> packs;
    private List<ResolveInfo> packsSearch;
    private ProgressDialog dialog;
    private Bundle data;
    private String mode;
    private String tabela;
    private Boolean selected;
    private ApplicationToBlockDAO appDAO;
    private AppPickerAdapter adapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        try{
            data = getArguments();
            mode = data.getString("mode");
            if(mode.equals("block")){
                tabela = MySQLiteHelper.TABLE_BLOCKEDAPPS;
            }
            else if(mode.equals("history")){
                tabela = MySQLiteHelper.TABLE_NOLOGAPPS;
            }
        }
        catch(Exception e){
            Log.i("AppManager error getting mode", e.toString());
            e.printStackTrace();
            Toast.makeText(getActivity(), getString(R.string.error_loading_apps), Toast.LENGTH_LONG).show();
        }

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        LoadApps la = new LoadApps();
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getString(R.string.loading));
        dialog.show();
        la.execute(null);

        final EditText searchField = (EditText) getActivity().findViewById(R.id.searchField);

        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 0) {
                    showApps(packs);
                } else {
                    packsSearch = new ArrayList<ResolveInfo>() {};
                    for (ResolveInfo pack : packs) {
                        Log.i("busca", pm.getApplicationLabel(pack.activityInfo.applicationInfo).toString().toLowerCase());
                        Log.i("busca", searchField.getEditableText().toString().toLowerCase());
                        Log.i("busca", "");
                        if (pm.getApplicationLabel(pack.activityInfo.applicationInfo).toString().toLowerCase().contains(searchField.getEditableText().toString().toLowerCase())) {
                            packsSearch.add(pack);
                        }
                    }

                    showApps(packsSearch);
                }
            }
        });
        //NavigationDrawerFragment.menuItem.setTitle(R.string.action_manage_blocks);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.blocked_app_manager, menu);
        inflater.inflate(R.menu.onoffbutton, menu);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if(prefs.getBoolean("blockModeActive", true)){
            menu.findItem(R.id.onOffBtn).setTitle(R.string.blocking_on);
        }
        if(prefs.getBoolean("historyModeActive", true)){
            menu.findItem(R.id.onOffBtnHistory).setTitle(R.string.history_on);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        appDAO = new ApplicationToBlockDAO(getActivity());
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        try {
            appDAO.open();
        } catch (Exception e) {
            Log.e("Database Error", e.toString());
        }

        AppManager appMng = new AppManager();
        Bundle data = new Bundle();

        switch (item.getItemId()) {
            case R.id.select_all:
                item.setTitle(R.string.selecting);

                dialog.setMessage(getString(R.string.selecting_apps));
                dialog.show();

                selected = appDAO.addAll(apps, tabela);
                if (selected) {
                    Toast.makeText(getActivity(), getString(R.string.success_selecting), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.fail_selecting), Toast.LENGTH_LONG).show();
                }
                data.putString("mode", "block");
                appMng.setArguments(data);

                ft.replace(R.id.container, appMng);
                dialog.dismiss();
                ft.commit();
                break;

            case R.id.clear_all:
                item.setTitle(R.string.clearing);

                selected = appDAO.clearAll(apps, tabela);

                if (selected) {
                    Toast.makeText(getActivity(), getString(R.string.success_clearing), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.fail_clearing), Toast.LENGTH_LONG).show();
                }

                data.putString("mode", "block");
                appMng.setArguments(data);

                ft.replace(R.id.container, appMng);
                ft.commit();
                break;
        }

        appDAO.close();
        return super.onOptionsItemSelected(item);
    }

    public void showApps(List<ResolveInfo> packs){
        if(packs.size()>0){
            apps = new ApplicationToBlock[packs.size()];
            ApplicationToBlock thisApp;
            int arraySize = 0;

        for (ResolveInfo pack : packs) {
                thisApp = new ApplicationToBlock();
                thisApp.setAppName(pm.getApplicationLabel(pack.activityInfo.applicationInfo).toString());
                thisApp.setPackageName(pack.activityInfo.packageName);
                thisApp.setIcon(pm.getApplicationIcon(pack.activityInfo.applicationInfo));
                thisApp.setBlocked(getActivity(), tabela);
                thisApp.setDateBlocked(pack.activityInfo.packageName, getActivity(), tabela);
                thisApp.setTable(tabela);

                apps[arraySize] = thisApp;
                arraySize++;
            }

            adapter = new AppPickerAdapter(getActivity(), apps);
            setListAdapter(adapter);

            getListView().setDivider(null);
            getListView().setDividerHeight(0);
        }
    }

    private class LoadApps extends AsyncTask<Void, Void, List<ResolveInfo>>{

        public LoadApps(){}

        @Override
        protected List<ResolveInfo> doInBackground(Void... params) {
            pm = getActivity().getPackageManager();
            Intent intent = new Intent(Intent.ACTION_MAIN, null);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            packs = pm.queryIntentActivities(intent, PackageManager.GET_META_DATA);

           Collections.sort(packs, new Comparator<ResolveInfo>() {
                @Override
                public int compare(final ResolveInfo object1, final ResolveInfo object2) {
                    return pm.getApplicationLabel(object1.activityInfo.applicationInfo).toString().compareToIgnoreCase(pm.getApplicationLabel(object2.activityInfo.applicationInfo).toString());
                }
            });

            return packs;
        }

        @Override
        protected void onPostExecute(List<ResolveInfo> result) {
            super.onPostExecute(result);
            showApps(result);
            dialog.dismiss();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        return inflater.inflate(R.layout.empty_listfragment_apppicker, null);
    }
}
