package com.vdlow.keepen.ui.fragments;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.BlockedWordDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.ui.libs.SwipeDismissListViewTouchListener;
import com.vdlow.keepen.ui.libs.UndoBarController;
import com.vdlow.keepen.classes.BlockedWord;
import com.vdlow.keepen.ui.activities.AddWordActivity;
import com.vdlow.keepen.ui.adapters.BlockedWordAdapter;

import java.util.List;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class BlockedWordsManager extends ListFragment implements UndoBarController.UndoListener {
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private BlockedWord word;
    private BlockedWordDAO wordDAO;
    private List<BlockedWord> words;
    private BlockedWordAdapter adapter;
    private BlockedWord wordRemoved;
    private int positionRemoved;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        wordDAO = new BlockedWordDAO(getActivity());
        words = wordDAO.getWordsBlocked();

        UndoBarController ubc = new UndoBarController(getActivity().findViewById(com.vdlow.keepen.R.id.undobar), this);
        ubc.hideUndoBar(true);

        if(!words.isEmpty()){
            adapter = new BlockedWordAdapter(getActivity(), words);
            getListView().setAdapter(adapter);
            getListView().setDivider(null);
            getListView().setDividerHeight(0);
            SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(getListView(),
                    new SwipeDismissListViewTouchListener.DismissCallbacks(){

                        @Override
                        public boolean canDismiss(int position) {
                            return true;
                        }

                        @Override
                        public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                            for(int position : reverseSortedPositions){
                                try{
                                    word = (BlockedWord)getListView().getAdapter().getItem(position);
                                    wordDAO = new BlockedWordDAO(getActivity());
                                    wordRemoved = wordDAO.deleteBlockedWord(word.getId());
                                    adapter.remove(word);
                                    adapter.notifyDataSetChanged();
                                    positionRemoved = position;

                                    UndoBarController mUbc = new UndoBarController(getActivity().findViewById(com.vdlow.keepen.R.id.undobar), BlockedWordsManager.this);
                                    mUbc.showUndoBar(false, getString(R.string.undo), null);

                                }
                                catch(Exception e){
                                    Log.i("NotificationsViewer", e.toString());
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), getString(R.string.ntf_false_exists), Toast.LENGTH_LONG);
                                }
                            }
                            Log.i("Dismissed", "dismiss");
                        }
                    }
            );
            getListView().setOnTouchListener(touchListener);
            getListView().setOnScrollListener(touchListener.makeScrollListener());

        }
        else{
            ((TextView)this.getListView().getEmptyView()).setText(getString(R.string.no_words_blocked));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.add_button, menu);
        inflater.inflate(R.menu.delete_all, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete_all:
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.delete))
                        .setMessage(getString(R.string.sure_delete_all))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                wordDAO = new BlockedWordDAO(getActivity());
                                wordDAO.deleteAllBlockedWords();
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                BlockedWordsManager blockedMng = new BlockedWordsManager();
                                ft.replace(R.id.container, blockedMng, "NtfViewer");
                                ft.commit();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
                        break;
            case R.id.add:
                Intent i = new Intent(getActivity(), AddWordActivity.class);
                startActivity(i);
                        break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        return inflater.inflate(R.layout.empty_listfragment, null);
    }

    @Override
    public void onUndo(Parcelable token) {
        wordDAO.addBlockedWord(word, getActivity());
        adapter.insert(wordRemoved, positionRemoved);
    }

    @Override
    public void onResume(){
        super.onResume();
        try{
            words = wordDAO.getWordsBlocked();
            if(!words.isEmpty()){
                adapter.clear();
                adapter.addAll(words);
                adapter.notifyDataSetChanged();
            }
        }
        catch(Exception e){
            Log.i("Exception onResume BlockedWordsManager", e.toString());
            e.printStackTrace();
        }
    }
}
