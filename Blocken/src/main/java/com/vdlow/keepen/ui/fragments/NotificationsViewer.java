package com.vdlow.keepen.ui.fragments;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vdlow.keepen.classes.DAO.NotificationBlockedDAO;
import com.vdlow.keepen.R;
import com.vdlow.keepen.ui.libs.EndlessScrollListener;
import com.vdlow.keepen.ui.libs.SwipeDismissListViewTouchListener;
import com.vdlow.keepen.ui.libs.UndoBarController;
import com.vdlow.keepen.classes.NotificationBlocked;
import com.vdlow.keepen.ui.adapters.NotificationBlockedAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class NotificationsViewer extends ListFragment implements UndoBarController.UndoListener {
    private PackageManager pm;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private List<NotificationBlocked> ntfs;
    private SharedPreferences prefs;
    private NotificationBlockedAdapter adapter;
    private NotificationBlockedDAO ntfDAO;
    private NotificationBlocked ntfRemoved;
    private int positionRemoved;
    private String isBlocked;
    private Bundle data;
    private String mode;
    private String groupBy;
    private String packageName;
    private int index = -1;
    private int top = 0;
    private int totalNtfs;
    private int receiver_container;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        if (getActivity().findViewById(R.id.container_detail) != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            SelectLeftFragment slf = new SelectLeftFragment();
            ft.replace(R.id.container_detail, slf, "AppManager");
            ft.commit();
            receiver_container = R.id.container_detail;
        } else {
            receiver_container = R.id.container;
        }

        try{
            data = getArguments();
            mode = data.getString("mode", "");
            if(mode.equals("block")) {
                isBlocked = "1";
            }
            else if(mode.equals("history")){
                isBlocked = "0";
            }
            groupBy = data.getString("groupBy", "");
            packageName = data.getString("package", "");
        }
        catch(Exception e){
            Log.i("NotificationsViewer error getting mode", e.toString());
            e.printStackTrace();
            Toast.makeText(getActivity(), getString(R.string.error_loading_ntfs), Toast.LENGTH_LONG).show();
        }

        UndoBarController ubc = new UndoBarController(getActivity().findViewById(com.vdlow.keepen.R.id.undobar), this);
        ubc.hideUndoBar(true);

        ntfDAO = new NotificationBlockedDAO(getActivity());

        try{
            if(groupBy.equals("perApp")) {
                ntfs = ntfDAO.getNotificationsBlocked(isBlocked, packageName, 0);
            }else{
                ntfs = ntfDAO.getNotificationsBlocked(isBlocked, 0);
            }
        }
        catch(Exception e){
            Log.i("NotificationsViewer error getting mode", e.toString());
            e.printStackTrace();
            ntfs = ntfDAO.getNotificationsBlocked(isBlocked, 0);
        }
        if(!ntfs.isEmpty()){
            adapter = new NotificationBlockedAdapter(getActivity(), ntfs);
            totalNtfs = ntfDAO.getNtfsNumber(Integer.parseInt(isBlocked), packageName);
            getListView().setAdapter(adapter);

            getListView().setDivider(null);
            getListView().setDividerHeight(0);

            SwipeDismissListViewTouchListener touchListener = new SwipeDismissListViewTouchListener(getListView(),
                    new SwipeDismissListViewTouchListener.DismissCallbacks(){

                        @Override
                        public boolean canDismiss(int position) {
                            return true;
                        }

                        @Override
                        public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                            for(int position : reverseSortedPositions){
                                try{
                                    NotificationBlocked ntf = (NotificationBlocked)getListView().getAdapter().getItem(position);
                                    NotificationBlockedDAO ntfDAO = new NotificationBlockedDAO(getActivity());
                                    ntfRemoved = ntfDAO.deleteNtfBlocked(ntf.getId());
                                    adapter.remove(ntf);
                                    adapter.notifyDataSetChanged();
                                    positionRemoved = position;

                                    UndoBarController mUbc = new UndoBarController(getActivity().findViewById(com.vdlow.keepen.R.id.undobar), NotificationsViewer.this);
                                    mUbc.showUndoBar(false, getString(R.string.undo), null);

                                }
                                catch(Exception e){
                                    Log.i("NotificationsViewer", e.toString());
                                    e.printStackTrace();
                                    Toast.makeText(getActivity(), getString(R.string.ntf_false_exists), Toast.LENGTH_LONG);
                                }
                            }
                            Log.i("Dismissed", "dismiss");
                        }
                    }
            );
            getListView().setOnTouchListener(touchListener);
            getListView().setOnScrollListener(new EndlessScrollListener() {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    if(totalItemsCount < totalNtfs){
                        index = getListView().getFirstVisiblePosition();
                        View v = getListView().getChildAt(0);
                        top = (v == null) ? 0 : v.getTop();
                        List<NotificationBlocked> ntfAux = new ArrayList<NotificationBlocked>(ntfs);
                        if(groupBy.equals("perApp")) {
                            ntfs = ntfDAO.getNotificationsBlocked(isBlocked, packageName, totalItemsCount);
                        }else{
                            ntfs = ntfDAO.getNotificationsBlocked(isBlocked, totalItemsCount);
                        }
                        ntfAux.addAll(ntfs);
                        ntfs = ntfAux;
                        adapter.clear();
                        adapter.addAll(ntfs);
                        adapter.notifyDataSetChanged();

                        if(index!=-1){
                            getListView().setSelectionFromTop(index, top);
                        }
                    }
                }
            });
        }else{
            ((TextView)this.getListView().getEmptyView()).setText(getString(com.vdlow.keepen.R.string.emptyNtfList));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        //inflater.inflate(com.vdlow.blocken.R.menu.main, menu);
        inflater.inflate(com.vdlow.keepen.R.menu.onoffbutton, menu);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        if(prefs.getBoolean("blockModeActive", true)){
            menu.findItem(com.vdlow.keepen.R.id.onOffBtn).setTitle(com.vdlow.keepen.R.string.blocking_on);
        }
        if(prefs.getBoolean("historyModeActive", true)){
            menu.findItem(R.id.onOffBtnHistory).setTitle(R.string.history_on);
        }
        inflater.inflate(R.menu.delete_all, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.delete_all:
                new AlertDialog.Builder(getActivity())
                        .setTitle(getString(R.string.delete))
                        .setMessage(getString(R.string.sure_delete_all))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                ntfDAO = new NotificationBlockedDAO(getActivity());
                                ntfDAO.deleteAllNtfs(isBlocked, packageName);
                                FragmentTransaction ft = getFragmentManager().beginTransaction();
                                Bundle data = new Bundle();
                                if(isBlocked.equals("1")){
                                    data.putString("mode", "blocked");
                                }
                                else if(isBlocked.equals("0")){
                                    data.putString("mode", "history");
                                }
                                if(groupBy.equals("perApp")){
                                    data.putString("package", packageName);
                                }
                                if (packageName != null && packageName.trim().length() > 0) {
                                    NtfsPerAppViewer ntfPerApp = new NtfsPerAppViewer();
                                    ntfPerApp.setArguments(data);
                                    ft.replace(R.id.container, ntfPerApp, "NtfPerAppViewer");
                                }
                                else{
                                    NotificationsViewer ntfViewer = new NotificationsViewer();
                                    ntfViewer.setArguments(data);
                                    ft.replace(com.vdlow.keepen.R.id.container, ntfViewer, "NtfViewer");
                                }
                                ft.commit();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        NotificationBlocked ntf = (NotificationBlocked)l.getAdapter().getItem(position);
        Bundle i = new Bundle();
        i.putInt("ntfID", ntf.getId());
        i.putString("ntfTitle", ntf.getNtfTitle());
        i.putString("ntfText", ntf.getNtfText());
        i.putString("ntfDate", ntf.getNtfDate());
        i.putString("ntfPackage", ntf.getApp().getPackageName());
        i.putString("mode", mode);
        i.putString("appName", ntf.getApp().getAppName());
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        NotificationFragment na = new NotificationFragment();
        na.setArguments(i);
        ft.replace(receiver_container, na, "NtfFragment");
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        super.onActivityCreated(savedInstanceState);
        return inflater.inflate(com.vdlow.keepen.R.layout.empty_listfragment, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        try{
            if(groupBy.equals("perApp")) {
                ntfs = ntfDAO.getNotificationsBlocked(isBlocked, packageName, 0);
            }else{
                ntfs = ntfDAO.getNotificationsBlocked(isBlocked, 0);
            }
            if(!ntfs.isEmpty()){
                adapter.clear();
                adapter.addAll(ntfs);
                adapter.notifyDataSetChanged();
                if(index!=-1){
                    this.getListView().setSelectionFromTop(index, top);
                }
            }
            data = getArguments();
        }
        catch(Exception e){
            Log.i("Exception onResume NotificationsViewer", e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void onUndo(Parcelable token) {
        adapter.insert(ntfRemoved, positionRemoved);
        ntfDAO.addNtfBlocked(ntfRemoved);
    }

    @Override
     public void onPause() {
        super.onPause();
        try{
            index = this.getListView().getFirstVisiblePosition();
            View v = this.getListView().getChildAt(0);
            top = (v == null) ? 0 : v.getTop();
        }
        catch(Exception e){
            Log.i("NotificationsViewer - error while saving position", e.toString());
            e.printStackTrace();
        }
    }
}
