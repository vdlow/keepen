package com.vdlow.keepen.classes;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.vdlow.keepen.classes.DAO.ApplicationToBlockDAO;

/**
 * Created by Vinicius Low on 29/11/13.
 */
public class ApplicationToBlock {

    private int id;
    private String packageName;
    private String appName;
    private boolean isBlocked;
    private String dateBlocked;
    private String table;
    private Drawable icon;

    public ApplicationToBlock(int id, String packageName, String appName, boolean isBlocked, Drawable icon) {
        this.id = id;
        this.packageName = packageName;
        this.appName = appName;
        this.isBlocked = isBlocked;
        this.icon = icon;
    }

    public ApplicationToBlock() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAppName() {
        return appName;
    }

    public String getAppName(String packageName, Context context){
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo;
        try {
            applicationInfo = packageManager.getApplicationInfo(packageName, 0);

            return (String)packageManager.getApplicationLabel(applicationInfo);
        }
        catch (Exception e){
            Log.e("Get app name Exception", e.toString());

            return "App name not found";
        }
        finally{
            context = null;
        }
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public boolean isBlocked(String packageName, Context context, String table) {
        isBlocked = false;
        ApplicationToBlockDAO appDAO = new ApplicationToBlockDAO(context);

        try{
            appDAO.open();
            isBlocked = appDAO.isAppBlocked(packageName, table);
            appDAO.close();
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
        }
        finally{
            context = null;
        }

        return isBlocked;
    }

    public void setBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }

    public void setBlocked(Context context, String table) {
        ApplicationToBlockDAO appDao = new ApplicationToBlockDAO(context);
        try {
            appDao.open();
            isBlocked = appDao.isAppBlocked(packageName, table);
        } catch (Exception e) {
            Log.e("Database Error", e.toString());
            isBlocked = false;
        } finally {
            appDao.close();
            context = null;
        }

    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getDateBlocked() {
        return dateBlocked;
    }

    public String getDateBlocked(String packageName, Context context, String table) {
        if (isBlocked) {
            ApplicationToBlockDAO appDao = new ApplicationToBlockDAO(context);
            try {
                appDao.open();
                dateBlocked = appDao.getDateBlocked(packageName, table);
            } catch (Exception e) {
                Log.e("Database Error", e.toString());
                dateBlocked = "";
            } finally {
                appDao.close();
                context = null;
            }
        }

        return dateBlocked;
    }

    public void setDateBlocked(String packageName, Context context, String table) {
        if (isBlocked) {
            ApplicationToBlockDAO appDao = new ApplicationToBlockDAO(context);
            try {
                appDao.open();
                dateBlocked = appDao.getDateBlocked(packageName, table);
            } catch (Exception e) {
                Log.e("Database Error", e.toString());
                dateBlocked = "";
            } finally {
                appDao.close();
                context = null;
            }
        }
        this.dateBlocked = dateBlocked;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
