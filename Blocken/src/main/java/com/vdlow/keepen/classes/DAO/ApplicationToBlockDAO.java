package com.vdlow.keepen.classes.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.vdlow.keepen.classes.ApplicationToBlock;

import java.sql.SQLException;
import java.util.Calendar;

/**
 * Created by Vinicius Low on 30/11/13.
 */
public class ApplicationToBlockDAO {

    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_PACKAGENAME,
            MySQLiteHelper.COLUMN_APPNAME, MySQLiteHelper.COLUMN_BLOCKEDDATE};

    public ApplicationToBlockDAO(Context context) {
        dbHelper = new MySQLiteHelper(context);
        context = null;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public boolean addAppBlock(String packageName, String appName, String table) {
        try {
            open();
            ContentValues values = new ContentValues();
            Calendar cal = Calendar.getInstance();
            String dataFinal = String.valueOf(
                    cal.get(Calendar.DAY_OF_MONTH) + "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR) +
                            " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE)
            );

            values.put(MySQLiteHelper.COLUMN_PACKAGENAME, packageName);
            values.put(MySQLiteHelper.COLUMN_APPNAME, appName);
            values.put(MySQLiteHelper.COLUMN_BLOCKEDDATE, dataFinal);

            database.insert(table, null, values);

            return true;
        } catch (Exception e) {
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return false;
        }
        finally {
            close();
        }
    }

    public void deleteAppBlock(String packageName, String table) {
        try {
            open();
            database.delete(table, MySQLiteHelper.COLUMN_PACKAGENAME + " = '" + packageName + "'", null);
        } catch (Exception e) {
            Log.e("Database Error", e.toString());
        }
        finally {
            close();
        }
    }

    public boolean isAppBlocked(String packageName, String table) {
        boolean isBlocked = false;
        try {
            open();
            Cursor cursor = database.query(table, new String[]{MySQLiteHelper.COLUMN_ID}, MySQLiteHelper.COLUMN_PACKAGENAME + " = '" + packageName + "'", null, null, null, null);

            if (cursor.getCount() > 0) {
                isBlocked = true;
            }
        } catch (Exception e) {
            Log.e("Database Error", e.toString());
            e.printStackTrace();
        }
        finally{
            close();
            return isBlocked;
        }
    }

    public String getDateBlocked(String packageName, String table) {
        String dateBlocked = "";
        Cursor cursor = database.query(table, new String[]{MySQLiteHelper.COLUMN_BLOCKEDDATE}, MySQLiteHelper.COLUMN_PACKAGENAME + " = '" + packageName + "'", null, null, null, null);

        if (cursor.getCount() > 0) {
            cursor.moveToNext();
            dateBlocked = cursor.getString(0);
            cursor.close();
        }

        return dateBlocked;
    }

    public boolean addAll(ApplicationToBlock[] apps, String table) {
       for (ApplicationToBlock app : apps) {
            Log.i("nomepac", app.getPackageName() + app.getAppName());
            addAppBlock(app.getPackageName(), app.getAppName(), table);
        }

        return true;
    }

    public boolean clearAll(ApplicationToBlock[] apps, String table) {
        for (ApplicationToBlock app : apps) {
            deleteAppBlock(app.getPackageName(), table);
        }

        return true;
    }

    public ApplicationToBlock cursorToApp(Cursor cursor, Context context, String table) {
        ApplicationToBlock app = new ApplicationToBlock();
        app.setAppName(cursor.getString(2));
        app.setPackageName(cursor.getString(1));
        app.setBlocked(true);
        app.setId(cursor.getInt(0));
        app.setDateBlocked(cursor.getString(3), context, table);
        context = null;

        return app;
    }

    public int getNumberApps(String table){
        try{
            open();
            Cursor cursor = database.query(table, new String[]{MySQLiteHelper.COLUMN_ID}, null, null, null, null, null);
            return cursor.getCount();
        }
        catch (Exception e) {
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return 0;
        }
        finally{
            close();
        }
    }
}
