package com.vdlow.keepen.classes;

import android.annotation.TargetApi;
import android.app.Notification;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.DAO.BlockedWordDAO;
import com.vdlow.keepen.classes.DAO.MySQLiteHelper;
import com.vdlow.keepen.classes.DAO.NotificationBlockedDAO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static com.vdlow.keepen.classes.DateUtils.*;

/**
 * Created by Vinicius Low on 01/12/13.
 */
@TargetApi(18)
public class NotificationListener extends NotificationListenerService {

    private ApplicationToBlock app;
    private Notification n;
    private SharedPreferences prefs;
    private NotificationBlockedDAO ntfDAO;
    private NotificationBlocked ntf;
    private BlockedWordDAO wordDAO;
    private StatusBarNotification sbn;
    private Boolean shouldMute = false;
    private SimpleDateFormat dFormat;
    private Calendar cStart;
    private Calendar cStop;
    private Calendar cNow;

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Log.i("Notification Listener", "ativado");
        app = new ApplicationToBlock();
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (isMuteTime() && prefs.getBoolean("muteActive", false)) {
            shouldMute = true;
        }
        else{
            shouldMute = false;
        }

        if(prefs.getBoolean("blockModeActive", true) || prefs.getBoolean("historyModeActive", true) || shouldMute){
            Log.i("Notification Listener", "entrou no if");
            this.sbn = sbn;
            Log.i("Notification Listener", "setou sbn");
            n = sbn.getNotification();

            ntfDAO = new NotificationBlockedDAO(getApplicationContext());
            ntf = new NotificationBlocked();
            app = new ApplicationToBlock();

            app.setPackageName(sbn.getPackageName());
            app.setAppName(app.getAppName(app.getPackageName(), getApplicationContext()));

            Log.i("Listener - App name and package", app.getPackageName() + " / " + app.getAppName());

            if(Build.VERSION.SDK_INT == 18){
                createNotificationJB();
            }
            else{
                createNotificationKitKat();
            }

            if(shouldMute){
                ntf.setIsMutedNtf(1);
            }

            if(prefs.getBoolean("blockModeActive", true) && !shouldMute) {
                wordDAO = new BlockedWordDAO(getApplicationContext());
                List<BlockedWord> words = wordDAO.getWordsBlocked();
                if (app.isBlocked(sbn.getPackageName(), getApplicationContext(), MySQLiteHelper.TABLE_BLOCKEDAPPS)) {
                    Log.i("Notification Listener", "bloqueada");
                    ntf.setIsBlocked(1);
                    ntfDAO.addNtfBlocked(ntf);

                    if (sbn.isClearable()) {
                        this.cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
                    }
                }else {
                    for (BlockedWord word : words) {
                        if(ntf.getNtfTitle().toLowerCase().contains(word.getWord().toLowerCase()) || (!word.getTitleOnly() && ntf.getNtfText().toLowerCase().contains(word.getWord().toLowerCase()))){
                            Log.i("Notification Listener", "bloqueada por palavra");
                            ntf.setIsBlocked(1);
                            ntfDAO.addNtfBlocked(ntf);

                            if (sbn.isClearable()) {
                                this.cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
                            }

                            break;
                        }
                    }
                }
            }
            if((prefs.getBoolean("historyModeActive", true) && !app.isBlocked(sbn.getPackageName(), getApplicationContext(), MySQLiteHelper.TABLE_NOLOGAPPS)) || shouldMute){
                if((!sbn.isClearable() && prefs.getBoolean("logPersistentNtfs", true)) || sbn.isClearable()){
                    Log.i("Notification Listener", "histórico");
                    ntf.setIsBlocked(0);
                    ntfDAO.addNtfBlocked(ntf);
                    if (shouldMute && sbn.isClearable()) {
                        this.cancelNotification(sbn.getPackageName(), sbn.getTag(), sbn.getId());
                    }
                }
            }

        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {

    }

    @TargetApi(18)
    public void createNotificationJB(){
        ntf.setApp(app);
        ntf.setNtfTitle(getString(R.string.touchToSeeMore));
        try{
            if (n.tickerText.toString().length() > 0) {
                ntf.setNtfText(n.tickerText.toString());
            }
        }
        catch(Exception e) {
            Log.i("Notification Ticker Text", e.toString());
            e.printStackTrace();
        }
        ntf.setNtfDate(getDate(sbn.getPostTime(), "yyyy/MM/dd HH:mm:ss"));
    }

    @TargetApi(19)
    public void createNotificationKitKat(){
        Bundle nInfos = n.extras;
        ntf.setApp(app);
        try{
            if(nInfos.getString(Notification.EXTRA_TITLE) != null && nInfos.getString(Notification.EXTRA_TITLE).trim().length() == 0){
                ntf.setNtfTitle(getString(R.string.touchToSeeMore));
            }
            else{
                ntf.setNtfTitle(nInfos.getString(Notification.EXTRA_TITLE));
            }
        }
        catch(Exception e) {
            Log.i("Notification Ticker Text", e.toString());
            e.printStackTrace();
            ntf.setNtfTitle(nInfos.getString(Notification.EXTRA_TITLE));
        }
        if(nInfos.getString(Notification.EXTRA_TEXT) != null && nInfos.getString(Notification.EXTRA_TEXT).trim().length()>0){
            ntf.setNtfText(nInfos.getString(Notification.EXTRA_TEXT));
        }
        else{
            try{
                if (n.tickerText.toString().length() > 0) {
                    ntf.setNtfText(n.tickerText.toString());
                }
            }
            catch(Exception e) {
                Log.i("Notification Ticker Text", e.toString());
                e.printStackTrace();
            }
        }
        ntf.setNtfDate(getDate(sbn.getPostTime(), "yyyy/MM/dd HH:mm:ss"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //DO NOTHING
    }

    private Boolean isMuteTime() {
        if(prefs.getInt("startMuteHour", -1) >= 0 && prefs.getInt("startMuteMinute", -1) >= 0
                && prefs.getInt("stopMuteHour", -1) >= 0 && prefs.getInt("stopMuteMinute", -1) >= 0) {

            if(android.text.format.DateFormat.is24HourFormat(getApplicationContext())){
                dFormat = new SimpleDateFormat("H:mm");
            }
            else{
                dFormat = new SimpleDateFormat("h:mm a");
            }

            try{
                cStart = Calendar.getInstance();
                cStart.set(0, 0, 0, prefs.getInt("startMuteHour", -1), prefs.getInt("startMuteMinute", -1));
                cStart.setTime(dFormat.parse(dFormat.format(cStart.getTime())));

                cStop = Calendar.getInstance();
                cStop.set(0, 0, 0, prefs.getInt("stopMuteHour", -1), prefs.getInt("stopMuteMinute", -1));
                cStop.setTime(dFormat.parse(dFormat.format(cStop.getTime())));

                cNow = Calendar.getInstance();
                cNow.setTime(dFormat.parse(dFormat.format(cNow.getTime())));
            }
            catch(ParseException e){
                Log.i("Error parsing time", e.toString());
                e.printStackTrace();
            }
            if(cNow.getTime().after(cStart.getTime()) && cNow.getTime().before(cStop.getTime())){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
}
