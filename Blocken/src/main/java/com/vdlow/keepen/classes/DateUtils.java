package com.vdlow.keepen.classes;

import android.content.Context;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Vinicius Low on 03/02/14.
 */
public class DateUtils {
    public static String getDate(long milliSeconds, String dateFormat)
    {
        // Create a DateFormatter object for displaying date in specified format.
        DateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDate(String date, Context context) {
        // Create a DateFormatter object for displaying date in specified format.

        DateFormat formatterDate = android.text.format.DateFormat.getDateFormat(context);
        DateFormat formatterHour = android.text.format.DateFormat.getTimeFormat(context);
        DateFormat oldFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(oldFormatter.parse(date));
        } catch (ParseException e) {
            Log.i("Error parsing date", e.toString());
            e.printStackTrace();
        }
        return formatterDate.format(calendar.getTime()) + " " + formatterHour.format(calendar.getTime());
    }

}
