package com.vdlow.keepen.classes;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.vdlow.keepen.billingsutils.IabHelper;
import com.vdlow.keepen.billingsutils.IabResult;
import com.vdlow.keepen.billingsutils.Inventory;
import com.vdlow.keepen.billingsutils.Purchase;
import com.vdlow.keepen.ui.activities.WelcomeActivity;

import java.util.ArrayList;

/**
 * Created by Vinicius Low on 31/01/14.
 */
public class IAPService extends Service {
    private final IBinder mBinder = new MyBinder();
    private IabHelper mHelper;
    public static final String ITEM_SKU = "com.vdlow.keepen.pro";
    public static final int REQUEST_CODE = 2320310;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    public static Boolean isRunning;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgge0rIzWi/AiGqEBUygKeRfuW8XrscTRkbml02SYw+76enneDMAkOGqHmGLC9r85eOxvhXVRd+JMG2UXU4SGuokpf2ulBKDOpcFS/Hu1NjrkbyZLoYtPGR/DC+vmIYkOfS82cwDswduc6Iqo1YQT9FPaT1NhyA1tAgyG/dJKsEgajHEheQN5WgdKHF50lfmkInqWHsUVhz1VKXE51S6H4g7uuPCFGjufLjchl5N5lRYcFbq5IMz+fF/seo7DOUvKtHLYNW0X/0ZtDIarvCRFeUZkeIhZ5ibugt0tD1TbW3k9PluOiQ98KVZaInJ2k6QcyZQRCjTmt3NmurmicMZ5OQIDAQAB";

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = prefs.edit();
        if(isRunning == null || isRunning == false) {
            isRunning = true;
            try {
                mHelper = new IabHelper(getApplicationContext(), base64EncodedPublicKey);
                mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                    @Override
                    public void onIabSetupFinished(IabResult result) {
                        if (result.isSuccess()) {
                            Log.d("IAP", "In-app Billing setup ok: " +
                                    result);

                            ArrayList skus = new ArrayList();
                            skus.add(ITEM_SKU);
                            mHelper.queryInventoryAsync(mGotInventoryListener);
                        } else {
                            Log.d("IAP", "In-app Billing setup failed: " +
                                    result);
                        }
                    }
                });
            } catch (Exception e) {
                Log.i("Error IAPService", e.toString());
                e.printStackTrace();
            }
        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    public class MyBinder extends Binder {
        IAPService getService() {
            return IAPService.this;
        }
    }

    final IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inv) {
            Log.i("queryfinished", result.toString());
            if(inv != null){
                Purchase proPurchase = inv.getPurchase(ITEM_SKU);
                Boolean mIsPremium = (proPurchase != null && verifyDeveloperPayload(proPurchase));

                if(mIsPremium){
                    Log.i("premium", "yes");
                    editor.putBoolean("premiumpackage221293", true);
                }
                else{
                    Log.i("premium", "nope");
                    editor.putBoolean("premiumpackage221293", false);
                }
                editor.commit();
                Log.i("proPurchase", mIsPremium.toString());
                mHelper.dispose();
                isRunning = false;
            }
        }
    };

    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();
         /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         * */
        return true;
    }


    public static void goPro(final Activity activity) {
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgge0rIzWi/AiGqEBUygKeRfuW8XrscTRkbml02SYw+76enneDMAkOGqHmGLC9r85eOxvhXVRd+JMG2UXU4SGuokpf2ulBKDOpcFS/Hu1NjrkbyZLoYtPGR/DC+vmIYkOfS82cwDswduc6Iqo1YQT9FPaT1NhyA1tAgyG/dJKsEgajHEheQN5WgdKHF50lfmkInqWHsUVhz1VKXE51S6H4g7uuPCFGjufLjchl5N5lRYcFbq5IMz+fF/seo7DOUvKtHLYNW0X/0ZtDIarvCRFeUZkeIhZ5ibugt0tD1TbW3k9PluOiQ98KVZaInJ2k6QcyZQRCjTmt3NmurmicMZ5OQIDAQAB";
        final IabHelper mHelper = new IabHelper(activity, base64EncodedPublicKey);

        final IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if(result.isFailure()){
                    Log.d("IAP", "In-app Billing purchase failed: " +
                            result);
                }
                else if(info.getSku().equals(IAPService.ITEM_SKU)){
                    Log.d("IAP", "In-app Billing purchase worked: " +
                            result);

                    SharedPreferences prefs;
                    SharedPreferences.Editor editor;

                    prefs = PreferenceManager.getDefaultSharedPreferences(activity);
                    editor = prefs.edit();

                    editor.putBoolean("premiumpackage221293", true);
                    editor.commit();
                }
                mHelper.dispose();
            }
        };

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                if (result.isSuccess()) {
                    Log.d("IAP", "In-app Billing setup ok: " +
                            result);

                    mHelper.launchPurchaseFlow(activity, IAPService.ITEM_SKU, IAPService.REQUEST_CODE, mPurchaseFinishedListener, "");
                } else {
                    Log.d("IAP", "In-app Billing setup failed: " +
                            result);
                }
            }
        });
    }
}