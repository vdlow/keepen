package com.vdlow.keepen.classes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.DAO.MySQLiteHelper;
import com.vdlow.keepen.classes.DAO.NotificationBlockedDAO;

/**
 * This service class catches Toast or Notification of applications
 *
 * @author pankaj
 */
public class MyAccessibilityService extends AccessibilityService {

    private final AccessibilityServiceInfo info = new AccessibilityServiceInfo();
    private static final String TAG = "MyAccessibilityService";
    private ApplicationToBlock app;
    private NotificationBlocked ntf;
    private SharedPreferences prefs;
    private NotificationBlockedDAO ntfDAO;
    private Boolean shoudMute = false;
    private SimpleDateFormat dFormat;
    private Calendar cStart;
    private Calendar cStop;
    private Calendar cNow;

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

        final int eventType = event.getEventType();

        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (isMuteTime() && prefs.getBoolean("muteActive", false)) {
            shoudMute = true;
        }
        else{
            shoudMute = false;
        }

        if (eventType == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {
            if(prefs.getBoolean("historyModeActive", true) || shoudMute){

                app = new ApplicationToBlock();
                ntf = new NotificationBlocked();
                ntfDAO = new NotificationBlockedDAO(getApplicationContext());

                if(shoudMute){
                    ntf.setIsMutedNtf(1);
                }

                app.setPackageName((String) event.getPackageName());
                app.setAppName(app.getAppName(app.getPackageName(), getApplicationContext()));
                Parcelable parcelable = event.getParcelableData();


                if (parcelable instanceof Notification) {

                    Notification notification = (Notification) parcelable;
                    ntf.setApp(app);
                    ntf.setNtfTitle(getString(R.string.touchToSeeMore));

                    try{
                        if (notification.tickerText.toString().length() > 0) {
                            ntf.setNtfText(notification.tickerText.toString());
                        }
                    }
                    catch(Exception e) {
                        Log.i("Notification Ticker Text", e.toString());
                        e.printStackTrace();
                    }

                    ntf.setNtfDate(DateUtils.getDate(notification.when, "yyyy/MM/dd HH:mm:ss"));

                    if((prefs.getBoolean("historyModeActive", true) && !app.isBlocked(app.getPackageName(), getApplicationContext(), MySQLiteHelper.TABLE_NOLOGAPPS)) || shoudMute){
                        Log.i("Notification Listener", "histórico");
                        ntf.setIsBlocked(0);
                        ntfDAO.addNtfBlocked(ntf);
                    }

                }
            }
        } else {
            Log.v(TAG, "Got un-handled Event");
        }
    }

    @Override
    public void onInterrupt() {

    }

    @Override
    public void onServiceConnected() {
        // Set the type of events that this service wants to listen to.  
        //Others won't be passed to this service.
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;

        // If you only want this service to work with specific applications, set their
        // package names here.  Otherwise, when the service is activated, it will listen
        // to events from all applications.
        //info.packageNames = new String[]
        //{"com.appone.totest.accessibility", "com.apptwo.totest.accessibility"};

        // Set the type of feedback your service will provide.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        } else {
            info.feedbackType = AccessibilityServiceInfo.FEEDBACK_GENERIC;
        }

        // Default services are invoked only if no package-specific ones are present
        // for the type of AccessibilityEvent generated.  This service *is*
        // application-specific, so the flag isn't necessary.  If this was a
        // general-purpose service, it would be worth considering setting the
        // DEFAULT flag.

        // info.flags = AccessibilityServiceInfo.DEFAULT;

        info.notificationTimeout = 100;

        this.setServiceInfo(info);
    }

    public static final class Constants {

        public static final String EXTRA_MESSAGE = "extra_message";
        public static final String EXTRA_PACKAGE = "extra_package";
        public static final String ACTION_CATCH_TOAST = "com.mytest.accessibility.CATCH_TOAST";
        public static final String ACTION_CATCH_NOTIFICATION = "com.mytest.accessibility.CATCH_NOTIFICATION";
    }

    /**
     * Check if Accessibility Service is enabled. 
     *
     * @param mContext
     * @return <code>true</code> if Accessibility Service is ON, otherwise <code>false</code>
     */
    public static boolean isAccessibilitySettingsOn(Context mContext) {
        int accessibilityEnabled = 0;
        final String service = "com.mytest.accessibility/com.mytest.accessibility.MyAccessibilityService";

        boolean accessibilityFound = false;
        try {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.v(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        } catch (SettingNotFoundException e) {
            Log.e(TAG, "Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            Log.v(TAG, "***ACCESSIBILIY IS ENABLED*** -----------------");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                TextUtils.SimpleStringSplitter splitter = mStringColonSplitter;
                splitter.setString(settingValue);
                while (splitter.hasNext()) {
                    String accessabilityService = splitter.next();

                    Log.v(TAG, "-------------- > accessabilityService :: " + accessabilityService);
                    if (accessabilityService.equalsIgnoreCase(service)) {
                        Log.v(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        } else {
            Log.v(TAG, "***ACCESSIBILIY IS DISABLED***");
        }

        return accessibilityFound;
    }

    private Boolean isMuteTime() {
        if(prefs.getInt("startMuteHour", -1) >= 0 && prefs.getInt("startMuteMinute", -1) >= 0
                && prefs.getInt("stopMuteHour", -1) >= 0 && prefs.getInt("stopMuteMinute", -1) >= 0) {

            if(android.text.format.DateFormat.is24HourFormat(getApplicationContext())){
                dFormat = new SimpleDateFormat("H:mm");
            }
            else{
                dFormat = new SimpleDateFormat("h:mm a");
            }

            try{
                cStart = Calendar.getInstance();
                cStart.set(0, 0, 0, prefs.getInt("startMuteHour", -1), prefs.getInt("startMuteMinute", -1));
                cStart.setTime(dFormat.parse(dFormat.format(cStart.getTime())));

                cStop = Calendar.getInstance();
                cStop.set(0, 0, 0, prefs.getInt("stopMuteHour", -1), prefs.getInt("stopMuteMinute", -1));
                cStop.setTime(dFormat.parse(dFormat.format(cStop.getTime())));

                cNow = Calendar.getInstance();
                cNow.setTime(dFormat.parse(dFormat.format(cNow.getTime())));
            }
            catch(ParseException e){
                Log.i("Error parsing time", e.toString());
                e.printStackTrace();
            }
            if(cNow.getTime().after(cStart.getTime()) && cNow.getTime().before(cStop.getTime())){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }

    }
}