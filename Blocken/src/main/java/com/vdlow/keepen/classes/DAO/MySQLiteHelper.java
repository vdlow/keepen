package com.vdlow.keepen.classes.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Vinicius Low on 30/11/13.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "blockenblock.db";
    private static final int DATABASE_VERSION = 1;

    //TABELA DE APPS BLOQUEADOS
    public static final String TABLE_BLOCKEDAPPS = "blockedapps";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PACKAGENAME = "packagename";
    public static final String COLUMN_APPNAME = "appname";
    public static final String COLUMN_BLOCKEDDATE = "blockeddate";

    //TABELA DE PALAVRAS BLOQUEADAS
    public static final String TABLE_BLOCKEDWORDS = "blockedwords";
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_TITLE_ONLY = "titleonly";
    public static final String COLUMN_WORDDATE = "blockedworddate";

    //TABELA DE NOTIFICAÇÕES BLOQUEADAS
    public static final String TABLE_BLOCKEDNTF = "blockedntf";
    public static final String COLUMN_NTFTITLE = "ntftitle";
    public static final String COLUMN_NTFTEXT = "ntftext";
    public static final String COLUMN_NTFDATE = "ntfdate";
    public static final String COLUMN_NTFISBLOCKED = "isblocked";
    public static final String COLUMN_NTFISMUTED = "ismuted";

    //TABELA DE APPS QUE NÃO DEVEM ENTRAR NO HISTÓRICO DE NOTIFICAÇÕES
    public static final String TABLE_NOLOGAPPS = "nologapps";

    private static final String DATABASE_CREATE_DROP_TABLE_APPS =
            "DROP TABLE " + TABLE_BLOCKEDAPPS;

    private static final String DATABASE_CREATE_TABLE_APPS =
            "CREATE TABLE " + TABLE_BLOCKEDAPPS + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_PACKAGENAME + " TEXT NOT NULL, " + COLUMN_APPNAME + " TEXT NOT NULL, " + COLUMN_BLOCKEDDATE + " TEXT NOT NULL);";

    private static final String DATABASE_CREATE_DROP_TABLE_WORDS =
            "DROP TABLE " + TABLE_BLOCKEDWORDS;

    private static final String DATABASE_CREATE_TABLE_WORDS =
            "CREATE TABLE " + TABLE_BLOCKEDWORDS + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_WORD + " TEXT NOT NULL, " + COLUMN_WORDDATE + " TEXT NOT NULL, " + COLUMN_TITLE_ONLY + " INTEGER NOT NULL DEFAULT 0);";

    private static final String DATABASE_CREATE_DROP_TABLE_NTFS =
            "DROP TABLE " + TABLE_BLOCKEDNTF;

    private static final String DATABASE_CREATE_TABLE_NTFS =
            "CREATE TABLE " + TABLE_BLOCKEDNTF + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_NTFTITLE + " TEXT, " + COLUMN_NTFTEXT + " TEXT, "
                    + COLUMN_PACKAGENAME + " TEXT NOT NULL," + COLUMN_APPNAME + " TEXT NOT NULL," + COLUMN_NTFDATE + " TEXT NOT NULL, " +
                    COLUMN_NTFISMUTED + " INTEGER NOT NULL DEFAULT 0," + COLUMN_NTFISBLOCKED + " INTEGER NOT NULL DEFAULT 0);";

    private static final String DATABASE_CREATE_DROP_TABLE_NOLOGAPPS =
            "DROP TABLE " + TABLE_NOLOGAPPS;

    private static final String DATABASE_CREATE_TABLE_NOLOGAPPS =
            "CREATE TABLE " + TABLE_NOLOGAPPS + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_PACKAGENAME + " TEXT NOT NULL, " + COLUMN_APPNAME + " TEXT NOT NULL, " + COLUMN_BLOCKEDDATE + " TEXT NOT NULL);";


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_TABLE_APPS);
        db.execSQL(DATABASE_CREATE_TABLE_NTFS);
        db.execSQL(DATABASE_CREATE_TABLE_NOLOGAPPS);
        db.execSQL(DATABASE_CREATE_TABLE_WORDS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
