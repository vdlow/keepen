package com.vdlow.keepen.classes;

/**
 * Created by Vinicius Low on 02/12/13.
 */
public class NotificationBlocked {

    private int id;
    private String ntfTitle;
    private String ntfText;
    private String ntfDate;
    private ApplicationToBlock app;
    private int isBlockedNtf = 0;
    private int isMutedNtf = 0;
    public static int LIMIT_DISMISSED = 5;
    public static int LIMIT_HISTORY = 15;

    public NotificationBlocked() {
    }

    public NotificationBlocked(int id, String ntfTitle, String ntfText, String ntfDate) {
        this.id = id;
        this.ntfTitle = ntfTitle;
        this.ntfText = ntfText;
        this.ntfDate = ntfDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNtfTitle() {
        return ntfTitle;
    }

    public void setNtfTitle(String ntfTitle) {
        this.ntfTitle = ntfTitle;
    }

    public String getNtfText() {
        return ntfText;
    }

    public void setNtfText(String ntfText) {
        this.ntfText = ntfText;
    }

    public String getNtfDate() {
        return ntfDate;
    }

    public void setNtfDate(String ntfDate) {
        this.ntfDate = ntfDate;
    }

    public ApplicationToBlock getApp() {
        return app;
    }

    public void setApp(ApplicationToBlock app) {
        this.app = app;
    }

    public int getIsBlocked() {
        return isBlockedNtf;
    }

    public void setIsBlocked(int isBlockedNtf) {
        this.isBlockedNtf = isBlockedNtf;
    }

    public int getIsMutedNtf() {
        return isMutedNtf;
    }

    public void setIsMutedNtf(int isMutedNtf) {
        this.isMutedNtf = isMutedNtf;
    }
}
