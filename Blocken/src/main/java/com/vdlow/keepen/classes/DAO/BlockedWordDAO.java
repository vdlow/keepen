package com.vdlow.keepen.classes.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.vdlow.keepen.R;
import com.vdlow.keepen.classes.BlockedWord;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vinicius Low on 16/01/14.
 */
public class BlockedWordDAO {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_WORD,
            MySQLiteHelper.COLUMN_TITLE_ONLY, MySQLiteHelper.COLUMN_WORDDATE};
    private Context context;

    public BlockedWordDAO(Context context){
        dbHelper = new MySQLiteHelper(context);
        this.context = context;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public String addBlockedWord(BlockedWord word, Context context){
        try {
            if (wordExists(word.getWord())) {
                return context.getString(R.string.word_exists);
            }

            open();
            ContentValues values = new ContentValues();

            values.put(MySQLiteHelper.COLUMN_WORD, word.getWord());
            values.put(MySQLiteHelper.COLUMN_TITLE_ONLY, word.getTitleOnly());
            values.put(MySQLiteHelper.COLUMN_WORDDATE, word.getDate());

            database.insert(MySQLiteHelper.TABLE_BLOCKEDWORDS, null, values);

            Log.i("palavra inserida", values.toString());

            return context.getString(R.string.word_added);
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();

            return context.getString(R.string.word_not_added);
        }
        finally {
            close();
            context = null;
        }
    }

    public BlockedWord deleteBlockedWord(int id){
        try{
            BlockedWord word = getBlockedWord(String.valueOf(id));
            open();
            database.delete(MySQLiteHelper.TABLE_BLOCKEDWORDS, MySQLiteHelper.COLUMN_ID + " = " + id, null);
            return word;
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return null;
        }
        finally {
            close();
        }
    }
    public Boolean deleteAllBlockedWords(){
        try{
            open();
            database.delete(MySQLiteHelper.TABLE_BLOCKEDWORDS, null, null);
            return true;
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return false;
        }
        finally {
            close();
        }
    }

    public List<BlockedWord> getWordsBlocked(){
        List<BlockedWord> words = new ArrayList<BlockedWord>();

        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDWORDS, allColumns, null, null, null, null, MySQLiteHelper.COLUMN_WORDDATE + " DESC");
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                BlockedWord word = cursorToBlockedWord(cursor);
                words.add(word);
                cursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
        }
        finally {
            close();
            return words;
        }
    }

    public BlockedWord getBlockedWord(String id){
        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDWORDS, allColumns, MySQLiteHelper.COLUMN_ID + " = " + id, null, null, null, MySQLiteHelper.COLUMN_WORDDATE + " DESC");
            cursor.moveToFirst();
            return cursorToBlockedWord(cursor);
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return null;
        }
        finally {
            close();
        }
    }

    public Boolean changeTitleOnly(BlockedWord word){
        try{
            open();
            int isTitleOnly;
            if (word.getTitleOnly()) {
                isTitleOnly = 1;
            } else {
                isTitleOnly = 0;
            }

            ContentValues values = new ContentValues();
            values.put(MySQLiteHelper.COLUMN_TITLE_ONLY, isTitleOnly);

            database.update(MySQLiteHelper.TABLE_BLOCKEDWORDS, values, MySQLiteHelper.COLUMN_ID + " = " + String.valueOf(word.getId()), null);

            return true;
        }catch (Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return false;
        }
        finally {
            close();
        }
    }

    public BlockedWord cursorToBlockedWord(Cursor cursor){
        BlockedWord word = new BlockedWord();
        Boolean titleOnly;
        word.setId(cursor.getInt(0));
        word.setWord(cursor.getString(1));
        if(cursor.getInt(2) == 1) titleOnly = true; else titleOnly = false;
        word.setTitleOnly(titleOnly);
        word.setDate(cursor.getString(3));

        return word;
    }

    public Boolean wordExists(String word) {
        try {
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDWORDS, allColumns, MySQLiteHelper.COLUMN_WORD + " = '" + word + "'", null, null, null, null);
            if (cursor.getCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return false;
        }
        finally {
            close();
        }
    }

    public int getBlockedWordsNumber(){
        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDWORDS, allColumns, null, null, null, null, MySQLiteHelper.COLUMN_WORDDATE + " DESC");
            return cursor.getCount();
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return 0;
        }
        finally {
            close();
        }
    }

}
