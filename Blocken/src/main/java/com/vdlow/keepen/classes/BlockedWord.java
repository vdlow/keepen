package com.vdlow.keepen.classes;

/**
 * Created by Vinicius Low on 16/01/14.
 */
public class BlockedWord {
    private int id;
    private String word;
    private Boolean titleOnly;
    private String date;

    public BlockedWord(){}

    public BlockedWord(int id, String word, Boolean titleOnly) {
        this.id = id;
        this.word = word;
        this.titleOnly = titleOnly;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Boolean getTitleOnly() {
        return titleOnly;
    }

    public void setTitleOnly(Boolean titleOnly) {
        this.titleOnly = titleOnly;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
