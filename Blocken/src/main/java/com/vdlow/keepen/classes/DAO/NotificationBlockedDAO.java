package com.vdlow.keepen.classes.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;

import com.vdlow.keepen.classes.ApplicationToBlock;
import com.vdlow.keepen.classes.NotificationBlocked;
import com.vdlow.keepen.time.IAPChecker;

import java.io.File;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vinicius Low on 02/12/13.
 */
public class NotificationBlockedDAO {
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_NTFTITLE,
            MySQLiteHelper.COLUMN_NTFTEXT, MySQLiteHelper.COLUMN_NTFDATE, MySQLiteHelper.COLUMN_PACKAGENAME,
            MySQLiteHelper.COLUMN_APPNAME, MySQLiteHelper.COLUMN_NTFISBLOCKED, MySQLiteHelper.COLUMN_NTFISMUTED};
    private Context context;
    IAPChecker checker;
    private int finalLimit;
    final static int ntfsToLoad = 100;
    private String limit;
    private SharedPreferences prefs;

    public NotificationBlockedDAO(Context context){
        dbHelper = new MySQLiteHelper(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.context = context;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public boolean addNtfBlocked(NotificationBlocked nBlocked){
        try{
            if(!prefs.getBoolean("premiumpackage221293", false)){
                if(nBlocked.getIsBlocked() == 1){
                    verifyIfLimitReached(NotificationBlocked.LIMIT_DISMISSED, String.valueOf(nBlocked.getIsBlocked()), false);
                }
                else{
                    verifyIfLimitReached(NotificationBlocked.LIMIT_HISTORY, String.valueOf(nBlocked.getIsBlocked()), false);
                }
            }
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            Double dbSize = Double.parseDouble(getDbSize());
            if(dbSize.intValue() > Integer.parseInt(prefs.getString("maxDBSize", "0")) && !prefs.getString("maxDBSize", "0").equals("0")){
                verifyIfLimitReached(0, String.valueOf(nBlocked.getIsBlocked()), true);
            }
            open();
            ContentValues values = new ContentValues();

            values.put(MySQLiteHelper.COLUMN_NTFTITLE, nBlocked.getNtfTitle());
            values.put(MySQLiteHelper.COLUMN_NTFTEXT, nBlocked.getNtfText());
            values.put(MySQLiteHelper.COLUMN_NTFDATE, nBlocked.getNtfDate());
            values.put(MySQLiteHelper.COLUMN_PACKAGENAME, nBlocked.getApp().getPackageName());
            values.put(MySQLiteHelper.COLUMN_APPNAME, nBlocked.getApp().getAppName());
            values.put(MySQLiteHelper.COLUMN_NTFISBLOCKED, nBlocked.getIsBlocked());
            values.put(MySQLiteHelper.COLUMN_NTFISMUTED, nBlocked.getIsMutedNtf());

            Log.i("valores inseridos", values.toString());

            database.insert(MySQLiteHelper.TABLE_BLOCKEDNTF, null, values);

            return true;
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return false;
        }
        finally {
            close();
        }
    }

    public NotificationBlocked deleteNtfBlocked(int id){
        try{
            NotificationBlocked ntf = getNotificationBlocked(String.valueOf(id));
            open();
            database.delete(MySQLiteHelper.TABLE_BLOCKEDNTF, MySQLiteHelper.COLUMN_ID + " = " + id, null);
            return ntf;
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return null;
        }
        finally {
            close();
        }
    }

    public Boolean deleteAllNtfs(String isBlocked, String packageName){
        try {
            if (packageName != null && packageName.trim().length() > 0) {
                packageName = " AND " + MySQLiteHelper.COLUMN_PACKAGENAME + " = '" + packageName+ "'";
            }

            open();
            database.delete(MySQLiteHelper.TABLE_BLOCKEDNTF, MySQLiteHelper.COLUMN_NTFISBLOCKED + " = " + isBlocked + packageName, null);
            return true;
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return false;
        }
        finally {
            close();
        }
    }

    public List<NotificationBlocked> getNotificationsBlocked(String isBlocked, int previousLimit){
        List<NotificationBlocked> ntfs = new ArrayList<NotificationBlocked>();

        finalLimit = previousLimit + NotificationBlockedDAO.ntfsToLoad;

        if (previousLimit == -1) {
            limit = null;
        }
        else{
            limit = String.valueOf(previousLimit) + ','+ String.valueOf(finalLimit);
        }

        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDNTF, allColumns, MySQLiteHelper.COLUMN_NTFISBLOCKED + " = " + isBlocked, null, null, null, MySQLiteHelper.COLUMN_NTFDATE + " DESC", limit);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                NotificationBlocked ntf = cursorToNtf(cursor);
                ntfs.add(ntf);
                cursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
        }
        finally {
            close();
            Log.i("DAO", "retornou");
            return ntfs;
        }
    }
    public List<NotificationBlocked> getNotificationsBlocked(String isBlocked, String packageName, int previousLimit){
        List<NotificationBlocked> ntfs = new ArrayList<NotificationBlocked>();

        finalLimit = previousLimit + NotificationBlockedDAO.ntfsToLoad;

        if (previousLimit == -1) {
            limit = null;
        }
        else{
            limit = String.valueOf(previousLimit) + ','+ String.valueOf(finalLimit);
        }

        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDNTF, allColumns, MySQLiteHelper.COLUMN_NTFISBLOCKED + " = " + isBlocked + " AND " + MySQLiteHelper.COLUMN_PACKAGENAME + " = '" + packageName + "'", null, null, null, MySQLiteHelper.COLUMN_NTFDATE + " DESC", limit);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                NotificationBlocked ntf = cursorToNtf(cursor);
                ntfs.add(ntf);
                cursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
        }
        finally {
            close();
            Log.i("DAO", "retornou");
            return ntfs;
        }
    }

    public List<ApplicationToBlock> getApps(String isBlocked){
        List<ApplicationToBlock> apps = new ArrayList<ApplicationToBlock>();

        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDNTF, new String[]{MySQLiteHelper.COLUMN_PACKAGENAME, MySQLiteHelper.COLUMN_APPNAME}, MySQLiteHelper.COLUMN_NTFISBLOCKED + " = " + isBlocked, null, MySQLiteHelper.COLUMN_PACKAGENAME, null, MySQLiteHelper.COLUMN_APPNAME);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                ApplicationToBlock app = new ApplicationToBlock();
                app.setPackageName(cursor.getString(0));
                app.setAppName(cursor.getString(1));
                apps.add(app);
                cursor.moveToNext();
            }
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
        }
        finally {
            close();
            Log.i("DAO", "retornou");
            return apps;
        }
    }

    public NotificationBlocked getNotificationBlocked(String id){
        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDNTF, allColumns, MySQLiteHelper.COLUMN_ID + " = " + id, null, null, null, MySQLiteHelper.COLUMN_NTFDATE + " DESC");
            cursor.moveToFirst();
            return cursorToNtf(cursor);
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            return null;
        }
        finally {
            close();
        }
    }

    public void verifyIfLimitReached(int limit, String isBlocked, Boolean isDBSize){
        try{
            open();
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDNTF, allColumns, MySQLiteHelper.COLUMN_NTFISBLOCKED + " = " + isBlocked, null, null, null, MySQLiteHelper.COLUMN_NTFDATE + " DESC");
            if(cursor.getCount()>=limit || isDBSize){
                cursor.moveToLast();
                NotificationBlocked ntf = cursorToNtf(cursor);
                deleteNtfBlocked(ntf.getId());
            }
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
        }
        finally {
            close();
        }
    }

    public NotificationBlocked cursorToNtf(Cursor cursor){
        NotificationBlocked ntfBlocked = new NotificationBlocked();
        ntfBlocked.setId(cursor.getInt(0));
        ntfBlocked.setNtfTitle(cursor.getString(1));
        ntfBlocked.setNtfText(cursor.getString(2));
        ntfBlocked.setNtfDate(cursor.getString(3));
        ntfBlocked.setApp(new ApplicationToBlock());
        ntfBlocked.getApp().setPackageName(cursor.getString(4));
        ntfBlocked.getApp().setAppName(cursor.getString(5));
        ntfBlocked.setIsBlocked(cursor.getInt(6));
        ntfBlocked.setIsMutedNtf(cursor.getInt(7));
        return ntfBlocked;
    }

    public String getDbSize(){
        try{
            open();
            Double size = Double.parseDouble(String.valueOf(new File(database.getPath()).length()));
            size = size/1000000;
            return String.valueOf(new DecimalFormat("#0.00").format(size));
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return null;
        }
        finally {
            close();
        }
    }

    public int getNtfsNumber(int isBlocked, String packageName){
        try{
            String countPerPackage;
            open();
            if (packageName == null || packageName.trim().length() == 0) {
                countPerPackage = "";
            } else {
                countPerPackage = " AND " + MySQLiteHelper.COLUMN_PACKAGENAME + " = '" + packageName + "'";
            }
            Cursor cursor = database.query(MySQLiteHelper.TABLE_BLOCKEDNTF, allColumns, MySQLiteHelper.COLUMN_NTFISBLOCKED + " = " + String.valueOf(isBlocked) + countPerPackage, null, null, null, MySQLiteHelper.COLUMN_NTFDATE + " DESC");
            return cursor.getCount();
        }
        catch(Exception e){
            Log.e("Database Error", e.toString());
            e.printStackTrace();
            return 0;
        }
        finally {
            close();
        }
    }
}
