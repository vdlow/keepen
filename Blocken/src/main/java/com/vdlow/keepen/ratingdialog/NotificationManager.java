package com.vdlow.keepen.ratingdialog;

public interface NotificationManager {
    void showDialog(int flowPosition);
}
